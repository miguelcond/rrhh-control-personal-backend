## Paquetes requeridos

| Paquete | Descripción |
| ---------- | ---------- |
| git | Software para control de versiones |
| python | v2.7 |
| postgresql | v9.6 |
| apache | Servidor web |

## Requerimientos de Sistema Operativo

```sh
sudo apt-get install git
sudo apt-get install cron
sudo apt-get install apache2
sudo apt-get install libpq-dev
sudo apt-get install python-pip
sudo apt-get install python-dev
sudo apt-get install libxml2-dev libxslt1-dev
sudo apt-get install libjpeg-dev
sudo apt-get install zlib1g-dev
sudo ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib
sudo apt-get install libsasl2-dev libldap2-dev libssl-dev
sudo apt-get install libapache2-mod-wsgi
sudo pip install virtualenv
```

## Instalación de Postgres
```sh
sudo apt-get update
sudo apt-get install postgresql-9.6
sudo apt-get install postgresql-contrib postgresql-client-9.6
ps -ef | grep postgre
```
El último comando sólo es par comprobar la instalación.

### Problemas en la Autenticación
Al conectar con postgres, es posible que dispare el siguiente error:
```sh
$ psql -U postgres
Postgresql: password authentication failed for user “postgres”
ó
psql: FATAL:  la autentificación Peer falló para el usuario «postgres»
```
Si esto es así, se debe verificar los datos del siguiente archivo:
```sh
$ cd /etc/postgresql/9.6/main/
$ sudo nano pg_hba.conf
```
Como indica el enlace, la primera línea no comentada debería estar en `peer` o `ident`, en caso de que no sea así cambiarlo a estos valores, luego reiniciar el servicio:
```sh
$ sudo /etc/init.d/postgresql restart
```
Posteriormente, se procede a cambiar los datos del usuario postgres:
```sh
$ sudo -u postgres psql template1
$ ALTER USER postgres PASSWORD 'su-password';
```
Después, volver al archivo de configuración de postgres y cambiar peer por md5:
```sh
$ cd /etc/postgresql/9.6/main/
$ sudo nano pg_hba.conf
```
Ejemplo:
```sh
> # DO NOT DISABLE!
> # If you change this first entry you will need to make sure that the
# database superuser can access the database using some other method.
# Noninteractive access to all databases is required during automatic
# maintenance (custom daily cronjobs, replication, and similar tasks).
#
# Database administrative login by Unix domain socket
local   all             postgres                                md5

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
```
Reiniciar el servicio.
```sh
$ sudo /etc/init.d/postgresql restart
```
Con estos cambios ya es posible realizar la conexión con el siguiente comando:
```sh
$ psql -U postgres
Password for user postgres:
psql (9.6)
Type "help" for help.

postgres=#
```

## Guía de Instalación
* [INSTALL.md](INSTALL.md)
