"""agetic_rrhh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from control_personal import views
from rest_framework_jwt.views import obtain_jwt_token
from django.views import static
from django.conf import settings
from rest_framework.routers import DefaultRouter
# from rest_framework_proxy.views import ProxyView


router = DefaultRouter()
router.register(r'horarios', views.HorarioViewSet)
router.register(r'horario_rangos', views.HorarioRangoViewSet)
router.register(r'designacion_horarios', views.DesignacionHorarioViewSet)

router.register(r'registro_horarios', views.RegistroHorarioViewSet)
router.register(r'registro_horarios_revision', views.RegistroHorarioRevisionViewSet)
router.register(r'registro_horarios_revision_ultimo', views.RegistroHorarioRevisionUltimoViewSet)

router.register(r'feriados', views.FeriadoViewSet)
router.register(r'unidad_organizacional', views.UnidadOrganizacionalViewSet)
router.register(r'designacion_permisos', views.DesignacionPermisoViewSet)
router.register(r'designacion_vacacion', views.DesignacionVacacionViewSet)

router.register(r'designacion_permisos_funcionario', views.DesignacionPermisoFuncionarioViewSet)
router.register(r'designacion_permisos_jefe', views.DesignacionPermisoJefeViewSet)
router.register(r'designacion_permisos_rrhh', views.DesignacionPermisoRrhhViewSet)

router.register(r'historico_permisos', views.HistoricoPermisoViewSet)
router.register(r'historico_permisos_usuarios', views.HistoricoPermisoUsersViewSet)
router.register(r'historico_permisos_por_usuario', views.HistoricoPermisoByUserViewSet)
router.register(r'usuarios', views.UserViewSet)
router.register(r'todos_usuarios', views.AllUserViewSet)
router.register(r'todos_jefes', views.AllJefeViewSet)
router.register(r'configuracion_global', views.ConfiguracionGlobalViewSet)

router.register(r'sugerencias_post', views.SugerenciaPostViewSet)
router.register(r'sugerencias', views.SugerenciaViewSet)

router.register(r'designacion_vacacion_funcionario', views.DesignacionVacacionFuncionarioViewSet)
router.register(r'designacion_vacacion_jefe', views.DesignacionVacacionJefeViewSet)
# router.register(r'historico_vacacion_dependientes', views.HistoricoVacacionDependientesViewSet)

router.register(r'ficha_personal', views.FichaPersonalViewSet)
router.register(r'ficha_trayectoria', views.FichaTrayectoriaViewSet)
router.register(r'ficha_estudios_universitarios', views.FichaEstudiosUniversitariosViewSet)
router.register(r'ficha_estudios_especializacion', views.FichaEstudiosEspecializacionViewSet)
router.register(r'ficha_cursos_relacionados', views.FichaCursosRelacionadosViewSet)
router.register(r'ficha_idiomas', views.FichaIdiomasViewSet)
router.register(r'ficha_conocimientos', views.FichaConocimientosViewSet)
router.register(r'ficha_experiencia_laboral', views.FichaExperienciaLaboralViewSet)

router.register(r'ficha_personal_historico', views.FichaPersonalHistoricoViewSet)
# router.register(r'all_user', views.AllUserViewSet)


urlpatterns = [
    url(r'^pruebas/$', views.pruebas),
    # -------------------------------------------------------------------------------------------
    # PROXY REST
    # -------------------------------------------------------------------------------------------
    # url(r'^datos/$', ProxyView.as_view(source='api/v1/registros?inicio=2016-05-11'), name='item-list'),
    # url(r'^xdatos/$', ProxyView.as_view(source='http://127.0.0.1:8001/api/v2/archivos/'), name='item-list'),
    # url(r'^xdatos/$', ProxyView.as_view(source='api/v2/archivos/'), name='item-list'),
    # url(r'^xdatos/$', views.ItemListProxy.as_view(), name='item-list'),

    # -------------------------------------------------------------------------------------------
    # API REST
    # -------------------------------------------------------------------------------------------
    url(r'^api/v2/', include(router.urls)),
    # url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^admin/', admin.site.urls),
    # -------------------------------------------------------------------------------------------
    # API REST AUX
    # -------------------------------------------------------------------------------------------
    url(r'^api/v2/dias_laborales/$', views.DiasLaborales),
    url(r'^api/v2/dias_vacaciones/$', views.DiasVacacionRestantes),
    url(r'^api/v2/historico_vacacion_dependientes/$', views.HistoricoVacacionDependientes),
    # -------------------------------------------------------------------------------------------
    # JWT TOKEN
    # -------------------------------------------------------------------------------------------
    url(r'^api-token-auth/', obtain_jwt_token),
    # url(r'^confirmar/permiso/$', views.confirmar_permiso),
    # -------------------------------------------------------------------------------------------
    # SINCRONIZACION
    # -------------------------------------------------------------------------------------------
    url(r'^api/v2/revision/feriado/$', views.revision_feriado),
    url(r'^revision/horario/$', views.revision_horario),
    url(r'^api/v2/sincronizar/ldap/$', views.sincronizar_ldap),
    # url(r'^registro/funcionario/$', views.registro_funcionario),
    # -------------------------------------------------------------------------------------------
    # PROXY
    # -------------------------------------------------------------------------------------------
    url(r'^proxy/api/v1/registros$', views.RegistrosProxy.as_view()),
    url(r'^proxy/api/v1/biometricos$', views.BiometricoProxy.as_view()),
    url(r'^proxy/api/v1/registros_usuario$', views.RegistrosUsuarioProxy.as_view()),
    url(r'^proxy/api/v1/registros_usuario_dia$', views.RegistrosUsuarioDiaProxy.as_view()),
    url(r'^proxy/api/v1/registros_rango$', views.RegistrosRangoProxy.as_view()),
    url(r'^proxy/v1/feriados$', views.FeriadosProxy.as_view()),
    # -------------------------------------------------------------------------------------------
    # REPORTES
    # -------------------------------------------------------------------------------------------
    url(r'^reporte/marcaciones/individual/$', views.reporte_marcaciones, {'individual': True}),
    url(r'^reporte/marcaciones/todos/$', views.reporte_marcaciones, {'individual': False}),

    # url(r'^reporte/marcaciones/consolidado2/$', views.reporte_marcaciones_consolidado),
    url(r'^reporte/marcaciones/consolidado/$', views.reporte_marcaciones_consolidado2),
    url(r'^api/v2/designacion_permisos/print/(?P<id>[0-9]+)/$', views.report_permiso),
    url(r'^api/v2/designacion_permisos_funcionario/print/(?P<id>[0-9]+)/$', views.report_permiso),
    url(r'^api/v2/designacion_permisos_jefe/print/(?P<id>[0-9]+)/$', views.report_permiso),
    url(r'^api/v2/designacion_permisos_rrhh/print/(?P<id>[0-9]+)/$', views.report_permiso),

    url(r'^reporte/vacaciones/$', views.report_vacacion),
    url(r'^reporte/fichapersonal/$', views.report_fichapersonal),
    # (?P<token>\w+)/
    # -------------------------------------------------------------------------------------------
    # MEDIA PATH
    # -------------------------------------------------------------------------------------------
    url(r'^public/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT}),
]
