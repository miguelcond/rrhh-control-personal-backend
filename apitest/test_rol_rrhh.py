#!/usr/bin/python
# -*- coding: utf8 -*-
from rest_framework.test import APITestCase
from rest_framework_jwt.compat import get_user_model
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.test import APIClient
from control_personal.models import UnidadOrganizacional


User = get_user_model()
# ===========================================================================================
# JWT CREDENTIALS
# ===========================================================================================
API_username = 'usuario'
API_password = 'password'
API_email = 'usuario@institucion.gob.bo'
API_group = 'RRHH'
# -------------------------------------------------------------------------------------------


class RrhhAuthTest(APITestCase):
    def setUp(self):
        self.email = API_email
        self.username = API_username
        self.password = API_password
        self.user = User.objects.create_user(
            self.username, self.email, self.password)
        group = Group.objects.get(name=API_group)
        self.user.groups.add(group)
        self.data = {
            'username': self.username,
            'password': self.password
        }
        serializer = JSONWebTokenSerializer(data=self.data)
        self.is_valid = serializer.is_valid()
        self.token = serializer.object['token']
        # Colocar Autorization token a todos los request de esta sesion
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % format(self.token))

    # def valid_permission_all(self, url):
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     response = self.client.post(url, {})
    #     self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.get("%s%s/" % (url, self.user.id))
    #     self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.put("%s%s/" % (url, self.user.id), {})
    #     self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.delete("%s%s/" % (url, self.user.id))
    #     self.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def valid_permission_403(self, url):
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.post(url, {})
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.get("%s%s/" % (url, self.user.id))
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.put("%s%s/" % (url, self.user.id), {})
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #     response = self.client.delete("%s%s/" % (url, self.user.id))
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class RrhhHorariosAPITests(RrhhAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def test_can_read_usuarios_list(self):
        # response = self.client.get('/api/v2/horarios/', HTTP_AUTHORIZATION='Bearer {0}'.format(self.token))
        response = self.client.get('/api/v2/usuarios/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_read_horarios_list(self):
        response = self.client.get('/api/v2/horarios/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_add_and_read_horarios_detail(self):
        response = self.client.get('/api/v2/horarios/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_add_and_read_horarios_detail(self):
        data = {
            'descripcion': 'Horario de Prueba X',
            'observacion': 'Este horario es de prueba no tiene algun contenido extra'
        }
        response = self.client.post('/api/v2/horarios/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/api/v2/horarios/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_404_read_horarios_detail(self):
        response = self.client.get('/api/v2/horarios/20002/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_can_create_horarios(self):
        data = {
            'descripcion': 'Horario de Prueba',
            'observacion': 'Este horario es de prueba no tiene algun contenido extra'
        }
        response = self.client.post('/api/v2/horarios/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_update_horarios(self):
        data = {
            'descripcion': 'Horario Normal X',
            'observacion': 'Comentario cambiado'
        }
        response = self.client.put('/api/v2/horarios/1/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_delete_horarios(self):
        response = self.client.delete('/api/v2/horarios/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class RrhhHorarioRangosAPITests(RrhhAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def test_can_read_horariorangos_list(self):
        response = self.client.get('/api/v2/horario_rangos/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_create_horariorangos(self):
        data = {
            "descripcion": "Rango de Marcacion de Prueba",
            "hora_entrada": "08:30:00",
            "hora_salida": "12:00:00",
            "rango_entrada_ini": "00:01:00",
            "rango_entrada_fin": "11:59:00",
            "rango_salida_ini": "12:00:00",
            "rango_salida_fin": "13:29:00",
            "horario": 1
        }
        response = self.client.post('/api/v2/horario_rangos/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_valid_bad_create_horariorangos(self):
        data = {
            "descripcion": "Rango de Marcacion de Prueba",
            "hora_entrada": "18:30:00",
            "hora_salida": "12:00:00",
            "rango_entrada_ini": "13:01:00",
            "rango_entrada_fin": "11:59:00",
            "rango_salida_ini": "12:00:00",
            "rango_salida_fin": "13:29:00",
            "horario": 1
        }
        response = self.client.post('/api/v2/horario_rangos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class RrhhDesignacionHorariosAPITests(RrhhAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def test_can_read_designacionhorarios_list(self):
        response = self.client.get('/api/v2/designacion_horarios/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_create_designacionhorarios(self):
        # print User.objects.get(id=1).id
        data = {
            "usuario": self.user.id,
            "horario": 1,
            "fecha_ini": "2016-04-05",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_horarios/', data)
        # print response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionhorarios(self):
        data = {
            "usuario": self.user.id,
            "horario": 1,
            "fecha_ini": "2016-04-22",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_horarios/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class RrhhFeriadosAPITests(RrhhAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def test_can_read_feriados_list(self):
        response = self.client.get('/api/v2/feriados/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_create_feriados(self):
        data = {
            "descripcion": "cumpleaños de prueba",
            "fecha": "2016-04-06",
        }
        response = self.client.post('/api/v2/feriados/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class RrhhDesignacionPermisosAPITests(RrhhAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def setUp(self):
        super(RrhhDesignacionPermisosAPITests, self).setUp()
        # super(RrhhAuthTest, self).setUp()
        data = {
            "unidad_organizacional": "UNIDAD DE PRUEBA",
        }
        self.unidad, created = UnidadOrganizacional.objects.get_or_create(**data)
        response = self.client.post('/api/v2/unidad_organizacional/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_read_designacionpermisos_list(self):
        response = self.client.get('/api/v2/designacion_permisos/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # ...........................................................................................
    # COMISION
    # ...........................................................................................
    def test_can_create_designacionpermisos_comision(self):
        data = {
            "tipo_permiso": "comision",
            "unidad_organizacional": self.unidad.id,
            "motivo": "Motivo de prueba para justificacion de permiso",
            "comision_lugar": "Lugar de prueba - Hotel casa grande",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "estado": "creado",
            "hora_ini": "08:30:00",
            "hora_fin": "12:00:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-06",
        }
        # print self.user.id
        response = self.client.post('/api/v2/designacion_permisos/', data)
        # print response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_comision_case1(self):
        # Accidentalmente la fecha y hora no son validos para un rango inicio y final
        data = {
            "tipo_permiso": "comision",
            "unidad_organizacional": self.unidad.id,
            "motivo": "Motivo de prueba para justificacion de permiso",
            "comision_lugar": "Lugar de prueba - Hotel casa grande",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "hora_ini": "12:00:00",
            "hora_fin": "08:30:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-05",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_comision_case2(self):
        # Accidentalmente no se llenaron todos los campos requeridos
        data = {
            "tipo_permiso": "comision",
            "unidad_organizacional": self.unidad.id,
            "motivo": "Motivo de prueba para justificacion de permiso",
            "jefe": self.user.id,
            "hora_ini": "08:30:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # ...........................................................................................
    # PARTICULAR
    # ...........................................................................................
    def test_can_create_designacionpermisos_particular(self):
        data = {
            "tipo_permiso": "particular",
            "unidad_organizacional": self.unidad.id,
            "particular_numeral_iii": "Motivo de prueba para justificacion de permiso",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "hora_ini": "08:30:00",
            "hora_fin": "12:00:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_particular_case1(self):
        # Accidentalmente la fecha y hora no son validos para un rango inicio y final
        data = {
            "tipo_permiso": "particular",
            "unidad_organizacional": self.unidad.id,
            "particular_numeral_iii": "Motivo de prueba para justificacion de permiso",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "hora_ini": "13:00:00",
            "hora_fin": "08:30:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-04",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_particular_case2(self):
        # Accidentalmente no se llenaron todos los campos requeridos
        data = {
            "tipo_permiso": "particular",
            "unidad_organizacional": self.unidad.id,
            "particular_numeral_iv": "Motivo de prueba para justificacion de permiso",
            "jefe": self.user.id,
            "hora_ini": "08:30:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # ...........................................................................................
    # LICENCIA
    # ...........................................................................................
    def test_can_create_designacionpermisos_licencia(self):
        data = {
            "tipo_permiso": "licencia",
            "unidad_organizacional": self.unidad.id,
            "motivo": "Motivo de prueba para justificacion de la licencia",
            "licencia_respaldo": "Codigo de documento de respaldo Nº XYZ",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "fecha_ini": "2016-04-04",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        # print response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_licencia_case1(self):
        # Accidentalmente la fecha y hora no son validos para un rango inicio y final
        data = {
            "tipo_permiso": "licencia",
            "unidad_organizacional": self.unidad.id,
            "motivo": "Motivo de prueba para justificacion de la licencia",
            "licencia_respaldo": "Codigo de documento de respaldo Nº XYZ",
            "usuario": self.user.id,
            "jefe": self.user.id,
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-04",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_bad_create_designacionpermisos_licencia_case2(self):
        # Accidentalmente no se llenaron todos los campos requeridos
        data = {
            "tipo_permiso": "licencia",
            "unidad_organizacional": self.unidad.id,
            "licencia_numeral_iv": "Motivo de prueba para justificacion de permiso",
            "jefe": self.user.id,
            "hora_ini": "08:30:00",
            "fecha_ini": "2016-04-06",
            "fecha_fin": "2016-04-06",
        }
        response = self.client.post('/api/v2/designacion_permisos/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


# class RrhhAccessAPITests(RrhhAuthTest):
#     fixtures = ['auth_group', 'control_personal']
#
#     def test_can_access_historicopermisos(self):
#         response = self.client.get('/api/v2/historico_permisos/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def test_can_access_historicopermisosusuarios(self):
#         response = self.client.get('/api/v2/historico_permisos_usuarios/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def test_can_access_historicopermisosporusuario(self):
#         response = self.client.get('/api/v2/historico_permisos_por_usuario/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
