#!/usr/bin/python
# -*- coding: utf8 -*-
from rest_framework.test import APITestCase
from rest_framework_jwt.compat import get_user_model
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.test import APIClient


User = get_user_model()
# ===========================================================================================
# JWT CREDENTIALS
# ===========================================================================================
API_username = 'usuario'
API_password = 'password'
API_email = 'usuario@institucion.gob.bo'
API_group = 'FUNCIONARIO'
# -------------------------------------------------------------------------------------------


class FuncionarioAuthTest(APITestCase):
    def setUp(self):
        self.email = API_email
        self.username = API_username
        self.password = API_password
        self.user = User.objects.create_user(
            self.username, self.email, self.password)
        group = Group.objects.get(name=API_group)
        self.user.groups.add(group)
        self.data = {
            'username': self.username,
            'password': self.password
        }
        serializer = JSONWebTokenSerializer(data=self.data)
        self.is_valid = serializer.is_valid()
        self.token = serializer.object['token']
        # Colocar Autorization token a todos los request de esta sesion
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Bearer %s' % format(self.token))

    def valid_permission_403(self, url):
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.get("%s%s/" % (url, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.put("%s%s/" % (url, self.user.id), {})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete("%s%s/" % (url, self.user.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class FuncionarioAccessAPITests(FuncionarioAuthTest):
    fixtures = ['auth_group', 'control_personal']

    def test_cannot_access_usuarios(self):
        self.valid_permission_403('/api/v2/usuarios/')

    def test_cannot_access_horarios(self):
        self.valid_permission_403('/api/v2/horarios/')

    def test_cannot_access_horariosrangos(self):
        self.valid_permission_403('/api/v2/horario_rangos/')

    def test_cannot_access_designacionhorarios(self):
        self.valid_permission_403('/api/v2/designacion_horarios/')

    def test_cannot_access_registrohorarios(self):
        self.valid_permission_403('/api/v2/registro_horarios/')

    def test_cannot_access_feriados(self):
        self.valid_permission_403('/api/v2/feriados/')

    def test_cannot_access_unidadorganizacional(self):
        self.valid_permission_403('/api/v2/unidad_organizacional/')

    def test_cannot_access_designacionpermisos(self):
        self.valid_permission_403('/api/v2/designacion_permisos/')

    # def test_cannot_access_historicopermisos(self):
    #     response = self.client.get('/api/v2/historico_permisos/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_cannot_access_historicopermisosusuarios(self):
    #     response = self.client.get('/api/v2/historico_permisos_usuarios/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_can_access_historicopermisosporusuario(self):
    #     response = self.client.get('/api/v2/historico_permisos_por_usuario/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)


# class FuncionarioPermisoPITests(FuncionarioAuthTest):
#     fixtures = ['auth_group', 'control_personal']
#
#     def test_cannot_access_usuarios(self):


    # tengo que dumpear con configuracion global mas
    # ME QUEDE AQUI ENTONCES ME FALTA MIGRAR CON EL LDAP DE LA OFI Y LOS DATOS INICIALES
    # INCLUYENDO EL DE CONFIGURACION GLOBAL Y LAS UNIDADES ORGANIZACIONALES

    # def test_can_access_reportemarcaciones(self):
    #     response = self.client.get('/reporte/marcaciones/individual/?fecha_ini=09/06/2016&fecha_fin=11/06/2016')
    #     self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    # def test_cannot_access_historicopermisosusuarios(self):
    #     self.valid_permission_403('/api/v2/historico_permisos_usuarios/')
    #
    # def test_cannot_access_historicopermisosporusuario(self):
    #     self.valid_permission_403('/api/v2/historico_permisos_por_usuario/')
    #
    # def test_cannot_access_todosusuarios(self):
    #     self.valid_permission_403('/api/v2/todos_usuarios/')
