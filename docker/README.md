# Creación de la imagen

[Crear](https://docs.docker.com/engine/reference/commandline/build) la imagen **rrhh-backend**:

```sh
docker build -t rrhh-backend -f docker/Dockerfile .
```

# Uso de la imagen

## Despliegue básico

Se puede levantar un simple contenedor de prueba mediante el comando:

```sh
docker run --name rrhh-backend -d rrhh-backend
```

## Exposición de puerto externo

La imagen expone el puerto 8000


```sh
docker run --name rrhh-backend -d -p 8000:8000 rrhh-backend
```

## Variables de entorno

La imagen usa las siguientes variables de entorno:

```sh
# el secreto para la generacion de JWTs, es muy importante modificar esta cadena en produccion
JWT_SECRET=esta-cadena-tiene-que-cambiar

# el nombre de la base de datos
POSTGRES_NAME=backend

# el nombre de usuario de la base de datos asignada a la aplicacion
POSTGRES_USER=backend

# el password de la base de datos asignada a la aplicacion
POSTGRES_PASSWORD=backend

# el HOST de la base de datos, puede ser una direccion IP o un hostname
POSTGRES_HOST=rrhh-backend-db

# el puerto de la base de datos
POSTGRES_PORT=5432

# la URL del micro-servicio de integracion con los dispositivos biometricos
BIOMETRICOS_URL=http://<SERVIDOR-BIOMETRICOS>:5000

# la ruta de la API del micro-servicio de integracion con los dispositivos biometricos
BIOMETRICOS_AUTH=api/v1/tokens/

# el nombre de usuario de la API del micro-servicio de integracion con los dispositivos biometricos
BIOMETRICOS_USUARIO=admin

# el password de la API del micro-servicio de integracion con los dispositivos biometricos
BIOMETRICOS_PASSWORD=password

# la URL de la API del micro-servicio de feriados
FERIADOS_URL=http://<SERVIDOR-FERIADOS>:4000

# la URL del servicio de SMTP de la institucion
EMAIL_SMTP=smtp.institucion.gob.bo

# el puerto del servicio de SMTP de la institucion
EMAIL_PORT=587

# el nombre de usuario del servicio de SMTP de la institucion
EMAIL_USER=usuario@institucion.gob.bo

# el password del servicio de SMTP de la institucion
EMAIL_PASSWORD=password

# la URI del servicio de directorio LDAP de la institucion
LDAP_URI=ldaps://<SERVIDOR-LDAP>:636

# el DN del administrador del servicio de directorio LDAP de la institucion
LDAP_DN=cn=admin,dc=institucion,dc=gob,dc=bo

# el password de administrador del servicio de directorio LDAP de la institucion
LDAP_PASSWORD=password

# el nombre de la unidad organizacional de usuarios del servicio de directorio LDAP de la institucion
LDAP_USUARIOS=ou=usuarios,dc=institucion,dc=gob,dc=bo
```

## Despliegue mediante docker-compose.yml

Puedes levantar el servicio mediante un archivo [docker-compose.yml](https://docs.docker.com/compose/compose-file) incluyendo por ejemplo las siguientes líneas:

```yaml
backend:
  environment:
    BIOMETRICOS_URL: http://<SERVIDOR-BIOMETRICOS>:5000
    BIOMETRICOS_AUTH: api/v1/tokens/
    BIOMETRICOS_USUARIO: admin
    BIOMETRICOS_PASSWORD: password
    EMAIL_SMTP: smtp.institucion.gob.bo
    EMAIL_PORT: 587
    EMAIL_USER: user@institucion.gob.bo
    EMAIL_PASSWORD: password
    FERIADOS_URL: http://<SERVIDOR-FERIADOS>:4000
    JWT_SECRET: esta-cadena-tiene-que-cambiar-en-produccion
    LDAP_URI: ldaps://<SERVIDOR-LDAP>:636
    LDAP_DN: cn=admin,dc=institucion,dc=gob,dc=bo
    LDAP_PASSWORD: password
    LDAP_USUARIOS: ou=usuarios,dc=institucion,dc=gob,dc=bo
    POSTGRES_NAME: rrhh
    POSTGRES_USER: rrhh
    POSTGRES_PASSWORD: rrhh
    POSTGRES_HOST: backend_db
    POSTGRES_PORT: 5432
  image: rrhh-backend
  ports:
    - "8000:8000"
```
