#!/bin/env sh

set -e

CONTAINER_NAME=$1

echo "inicializando $CONTAINER_NAME"
docker exec $CONTAINER_NAME python manage.py migrate
docker exec $CONTAINER_NAME python manage.py loaddata control_personal
docker exec $CONTAINER_NAME python manage.py loaddata auth_group
docker exec -i -t $CONTAINER_NAME python manage.py createsuperuser
