# Manual de Instalación
Esta distribución fue llevada a cabo en una distribución linux Debian 9

## Configuración del Servidor
Para una correcta instalación, el servidor debe tener las siguientes configuraciones obligatoriamente:

> [SERVER.md](SERVER.md)

## Instalación del proyecto
### Creación de entorno virtual

```sh
virtualenv rrhh-control-personal-backend
cd rrhh-control-personal-backend
source bin/activate
git clone https://gitlab.agetic.gob.bo/agetic/rrhh-control-personal-backend.git
cd rrhh-control-personal-backend
```

### Instalación de Dependencias
```sh
pip install -r requirements.txt
```

### Archivo de configuración
```sh
cp agetic_rrhh/settings.py.dist agetic_rrhh/settings.py
```

### Configuración de LDAP
```
nano agetic_rrhh/settings.py
```

Reemplace la siguiente configuración por los parámetros que tiene implementado en su conexión de LDAP:

```sh
...
AUTH_LDAP_SERVER_URI = "ldaps://<SERVIDOR-LDAP>:636"
AUTH_LDAP_BIND_DN = "uid=<USERNAME_LDAP>,ou=usuarios,dc=institucion,dc=gob,dc=bo"
AUTH_LDAP_BIND_PASSWORD = "<PASSWORD_LDAP>"
X_LDAP_USERS = "ou=usuarios,dc=institucion,dc=gob,dc=bo"
X_LDAP_CLASS = "objectClass=inetOrgPerson"
X_LDAP_UID = "uid"
AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=usuarios,dc=institucion,dc=gob,dc=bo", ldap.SCOPE_SUBTREE, "(uid=%(user)s)")
AUTH_LDAP_GROUP_TYPE = PosixGroupType()
AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
    "email": "mail",
    "username": "uid",
    "cargo": "title",
}
...
```

Bajo los siguientes criterios:

```sh
AUTH_LDAP_SERVEX_LDAP_UIDR_URI: URL de conexión ldap
AUTH_LDAP_BIND_DN: Usuario con privilegio ver el árbol LDAP para usuarios
AUTH_LDAP_BIND_PASSWORD: contraseña de AUTH_LDAP_BIND_DN
X_LDAP_USERS: Base del directorio de usuarios
X_LDAP_CLASS: tipo de clase del directorio
X_LDAP_UID: Atributo que contiene el identificador único de usuario
AUTH_LDAP_USER_SEARCH: Busca en la base del directorio de usuario y en la coincidencias del identificador definido en X_LDAP_UID
AUTH_LDAP_USER_ATTR_MAP: parámetros de equivalencia para reconocer los nombres en la base de datos
    first_name: Parámetro del nombre completo
    last_name: Parámetro del apellido
    email: Correo electrónico
    username: Parámetro del Identificador único de usuario
    cargo: Parámetro donde esta el cargo
```


### Configuración de la Base de datos

#### Configuración base de datos:

Configure los parámetros de conexión donde se encuentra la base de datos

```
nano agetic_rrhh/settings.py
```

```
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'control_personal',
        'USER': 'control_personal',
        'PASSWORD': 'control_personal',
        'HOST': '<IP-BASE-DE-DATOS>',
        'PORT': '5432',
    }
}
...
```

#### Ejecución de migraciones en la base de datos:

Migraciones core base de django
```
python manage.py migrate
```

#### Cargar datos iniciales a la base de datos:

```
python manage.py loaddata auth_group
python manage.py loaddata control_personal
```

#### Creación del superusuario

```
python manage.py createsuperuser

Username (leave blank to use 'em'): admin
Email address: usuario@institucion.gob.bo
Password: password
Password (again): password
Superuser created successfully.
```

### Microservicios

Modifique las ip's y/o puertos, correspondiente a cada microservicio
```
nano agetic_rrhh/settings.py
```

```
...
REST_PROXY = {
    'BIOMETRICOS': {
        'HOST': 'http://<SERVIDOR-BIOMETRICOS>:5000',
        ...
    },
    'FERIADOS': {
        'HOST': 'http://<SERVIDOR-FERIADOS>:3000',
    }
}
...
```

### NOTA: Configuración de PATH

Cambie la ruta de acceso ___/home/usuario___, en las configuraciones mostradas a continuación, por la ruta donde realizo su instalación


### Configuración de APACHE

```
nano /etc/apache2/sites-enabled/000-default.conf
```

Agregue las siguientes lineas de código dentro de ___VirtualHost___, no olvide cambiar la configuración del PATH

```
Alias /static /home/usuario/rrhh-control-personal-backend/rrhh-control-personal-backend/agetic_rrhh/static
<Directory /home/usuario/rrhh-control-personal-backend/rrhh-control-personal-backend/agetic_rrhh/static>
   Require all granted
</Directory>

<Directory /home/usuario/rrhh-control-personal-backend/rrhh-control-personal-backend/agetic_rrhh>
   <Files wsgi.py>
       Require all granted
   </Files>
</Directory>
WSGIPassAuthorization On
WSGIDaemonProcess agetic_rrhh python-path=/home/usuario/rrhh-control-personal-backend:/home/usuario/rrhh-control-personal-backend/lib/python2.7/site-packages
WSGIProcessGroup agetic_rrhh
WSGIScriptAlias / /home/usuario/rrhh-control-personal-backend/rrhh-control-personal-backend/agetic_rrhh/wsgi.py
```

### Configuración de WSGI Django
```
cd agetic_rrhh/
cp wsgi.py.production.dist wsgi.py
nano wsgi.py
```

Cambiar la configuración del PATH del proyecto

```
import os, sys

sys.path.append('/home/usuario/rrhh-control-personal-backend/rrhh-control-personal-backend/')

os.environ['DJANGO_SETTINGS_MODULE'] = "agetic_rrhh.settings"
os.environ.setdefault("LANG", "en_US.UTF-8")
os.environ.setdefault("LC_ALL", "en_US.UTF-8")

activate_this = '/home/usuario/rrhh-control-personal-backend/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

```

### Desactivar errores en produccion

Editar el archivo
```
nano agetic_rrhh/settings.py
```

Reemplace la siguiente configuración por los parámetros que tiene implementado en su dominio:

```sh
...
DEBUG = False
ALLOWED_HOSTS = ['<SERVIDOR-INSTITUCION>']
...
```

### Configuración de crontab

Realizar la siguiente configuración para sincronización con los microservicios, `<SERVIDOR-BACKEND>` modifique por la ip donde instalo esta aplicación


```
crontab -e
```

```
*/30 * * * * curl --request GET 'http://<SERVIDOR-BACKEND>/revision/horario/'
*/30 * * * * curl --request GET 'http://<SERVIDOR-BACKEND>/api/v2/sincronizar/ldap/'
*/30 * * * * curl --request GET 'http://<SERVIDOR-BACKEND>/api/v2/revision/feriado/'
```

## Ejecutar las API Test

```
python manage.py test apitest/ -v 2
```
,