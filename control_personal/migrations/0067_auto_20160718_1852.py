# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-18 18:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('control_personal', '0066_designacionhorario_usuario'),
    ]

    operations = [
        migrations.CreateModel(
            name='DesignacionHorarioUsuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AlterField(
            model_name='designacionhorario',
            name='usuario',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='designacionhorariousuario',
            name='designacion_horario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='control_personal.DesignacionHorario'),
        ),
        migrations.AddField(
            model_name='designacionhorariousuario',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
