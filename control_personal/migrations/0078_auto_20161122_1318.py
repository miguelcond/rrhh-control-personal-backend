# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-22 13:18
from __future__ import unicode_literals

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('control_personal', '0077_configuracionglobal_form_enable_vacaciones'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuracionglobal',
            name='form_enable_vacaciones',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('enero', 'ENERO'), ('febrero', 'FEBRERO'), ('marzo', 'MARZO'), ('abril', 'ABRIL'), ('mayo', 'MAYO'), ('junio', 'JUNIO'), ('julio', 'JULIO'), ('agosto', 'AGOSTO'), ('septiembre', 'SEPTIEMBRE'), ('octubre', 'OCTUBRE'), ('noviembre', 'NOVIEMBRE'), ('diciembre', 'DICIEMBRE')], help_text='Las casillas no seleccionadas seran consideradas para reprogramacion', max_length=88, verbose_name='Meses habilitados para programacion de vacaciones'),
        ),
    ]
