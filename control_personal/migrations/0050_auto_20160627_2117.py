# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-27 21:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('control_personal', '0049_remove_registrohorariorevision_hora'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='unidadorganizacional',
            options={'permissions': (('view_unidadorganizacional', 'Puede ver los registros'), ('options_unidadorganizacional', 'Puede ver los options'))},
        ),
    ]
