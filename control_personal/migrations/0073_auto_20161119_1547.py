# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-19 15:47
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('control_personal', '0072_sugerencia'),
    ]

    operations = [
        migrations.CreateModel(
            name='DesignacionVacacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_vacacion', models.CharField(choices=[('completo', 'Todo el D\xeda'), ('medio', 'Medio D\xeda'), ('horas', 'Horas')], max_length=15, verbose_name='Tipo')),
                ('fecha_ini', models.DateField(verbose_name='Fecha Inicial (DD/MM/YYYY)')),
                ('fecha_fin', models.DateField(verbose_name='Fecha Final (DD/MM/YYYY)')),
                ('hora_ini', models.TimeField(blank=True, null=True, verbose_name='Hora de salida (HH:MM)')),
                ('hora_fin', models.TimeField(blank=True, null=True, verbose_name='Hora de retorno (HH:MM)')),
                ('jefe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jefe_vacacion', to=settings.AUTH_USER_MODEL)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('view_vacacion', 'Puede ver los registros'), ('options_vacacion', 'Puede ver los options')),
            },
        ),
        migrations.AddField(
            model_name='configuracionglobal',
            name='horas_laborales_min',
            field=models.IntegerField(default=8, verbose_name='Horas laborales minima'),
        ),
    ]
