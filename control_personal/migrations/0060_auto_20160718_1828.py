# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-18 18:28
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control_personal', '0059_auto_20160718_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='designacionhorario',
            name='usuario',
            field=models.ManyToManyField(help_text='{prueba}', related_name='sponsored_segments', to=settings.AUTH_USER_MODEL),
        ),
    ]
