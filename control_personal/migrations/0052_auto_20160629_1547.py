# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-29 15:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control_personal', '0051_auto_20160629_1535'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='designacionpermiso',
            name='aprobado',
        ),
        migrations.AddField(
            model_name='designacionpermiso',
            name='estado',
            field=models.CharField(choices=[('creado', 'Creado'), ('aprobado jefe', 'Aprobado Jefe'), ('aprobado rrhh', 'Aprobado RRHH')], default='creado', max_length=15, verbose_name='Estado'),
            preserve_default=False,
        ),
    ]
