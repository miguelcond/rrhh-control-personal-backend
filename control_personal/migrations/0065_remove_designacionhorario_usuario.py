# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-18 18:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('control_personal', '0064_designacionhorario_usuario'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='designacionhorario',
            name='usuario',
        ),
    ]
