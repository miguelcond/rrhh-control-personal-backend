import requests
import datetime
from django.utils import timezone
from django.conf import settings


def get_auth_header_api(xsettings):
    headers = {}
    xhost = xsettings['HOST']
    xurl_token = xsettings['AUTH']['auth_url']
    post_data = xsettings['AUTH']['auth_credentials']
    auth_header_key = xsettings['AUTH']['auth_header_key']
    post_request = requests.post("/".join([xhost, xurl_token]), data=post_data)
    if "token" in post_request.json():
        xprefix = xsettings['AUTH']['jwt_auth_header_prefix']
        headers[auth_header_key] = " ".join([xprefix, post_request.json()['token']]).strip()
    return headers


def get_service_api_registros(fecha_gte):
    xsettings = settings.REST_PROXY['BIOMETRICOS']
    headers = get_auth_header_api(xsettings)
    registros = {}
    if len(headers) >= 1:
        dias = (datetime.date.today() - fecha_gte).days
        if dias < 0:
            dias = 0
        requests.get('%s/api/v1/recuperacion_registros/?dias=%s' % (xsettings['HOST'], dias), headers=headers)
        api_rest = requests.get('%s/api/v1/registros/?inicio=%s' % (xsettings['HOST'], fecha_gte.strftime('%Y-%m-%d')), headers=headers)
        registros = api_rest.json()
        print registros
    return registros


def get_service_api_registros_rango(fecha_ini, fecha_fin):
    xsettings = settings.REST_PROXY['BIOMETRICOS']
    headers = get_auth_header_api(xsettings)
    registros = {}
    if len(headers) >= 1:
        requests.get('%s/recuperacion_registros/' % xsettings['HOST'])
        api_rest = requests.get('%s/registros/?inicio=%s&fin=%s' % (xsettings['HOST'], fecha_ini.strftime('%Y-%m-%d'), fecha_fin.strftime('%Y-%m-%d')), headers=headers)
        registros = api_rest.json()
    return registros


def get_service_api_feriados():
    xsettings = settings.REST_PROXY['FERIADOS']
    api_rest = requests.get('%s/v1/feriados?pais=BO&ano=%s' % (xsettings['HOST'], timezone.now().year))
    return api_rest.json()['feriados']
