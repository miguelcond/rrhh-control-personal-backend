#!/usr/bin/python
# -*- coding: utf8 -*-
from models import User, Horario, HorarioRango, DesignacionHorario, RegistroHorario, RegistroHorarioRevision, Feriado, UnidadOrganizacional, DesignacionPermiso, DesignacionVacacion, ConfiguracionGlobal
from models import Sugerencia
from models import FichaPersonal, FichaTrayectoria, FichaEstudiosUniversitarios, FichaEstudiosEspecializacion, FichaCursosRelacionados, FichaIdiomas, FichaIdiomas, FichaConocimientos, FichaExperienciaLaboral, FichaPersonalHistorico
from rest_framework import serializers
# from rest_framework.fields import CurrentUserDefault


import re


def not_special_chars(vinput):
    return re.match('[^;\'<>`~]+$', vinput)
    # return re.match('[^,.;\'<>`~]+$', vinput)


class SugerenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sugerencia

    def create(self, validated_data):
        #HTTP_X_REAL_IP
        validated_data['ipaddr'] = self.context['request'].META.get('REMOTE_ADDR')
        return Sugerencia.objects.create(**validated_data)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario

    def validate(self, attrs):
        if attrs.get('tolerancia') < 0:
            raise serializers.ValidationError({'Tolerancia en minutos': 'La tolerancia debe ser mayor o igual que 0'})

        if not not_special_chars(attrs.get('descripcion')):
            raise serializers.ValidationError({'Descripción': 'No se permiten carácteres especiales'})

        if not not_special_chars(attrs.get('observacion')):
            raise serializers.ValidationError({'Observación': 'No se permiten carácteres especiales'})

        return attrs


class HorarioRangoSerializer(serializers.ModelSerializer):
    class Meta:
        model = HorarioRango

    def validate(self, attrs):
        if attrs.get('hora_entrada') <= attrs.get('rango_entrada_ini'):
            raise serializers.ValidationError({'Horario Rango': 'Error la hora de entrada debe ser mayor al rango de entrada de inicio'})

        if attrs.get('hora_entrada') > attrs.get('hora_salida'):
            raise serializers.ValidationError({'Horario Rango': 'Error la hora de entrada debe ser menor o igual a la hora de salida'})

        if attrs.get('hora_salida') >= attrs.get('rango_salida_fin'):
            raise serializers.ValidationError({'Horario Rango': 'Error la hora de salida debe ser menor al rango de salida final'})

        if attrs.get('rango_entrada_ini') > attrs.get('rango_entrada_fin'):
            raise serializers.ValidationError({'Horario Rango': 'Error el rango de entrada de inicio debe ser menor o igual al rango de entrada final'})

        if attrs.get('rango_entrada_fin') >= attrs.get('rango_salida_ini'):
            raise serializers.ValidationError({'Horario Rango': 'Error el rango de entrada debe ser menor al rango de salida'})

        if attrs.get('rango_salida_ini') > attrs.get('rango_salida_fin'):
            raise serializers.ValidationError({'Horario Rango': 'Error el rango de salida de inicio debe ser menor o igual al rango de salida final'})

        if not not_special_chars(attrs.get('descripcion')):
            raise serializers.ValidationError({'Descripción': 'No se permiten carácteres especiales'})

        return attrs


class DesignacionHorarioSerializer(serializers.ModelSerializer):
    usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True, many=True)
    # usuario = serializers.ManyRelatedField(allow_null=False, child_relation=User.objects.filter(habilitado_marcar=True), required=True)

    # def validate(self, attrs):
    #     instance = DesignacionHorario(**attrs)
    #     instance.clean()
    #     return attrs

    class Meta:
        model = DesignacionHorario

    def validate(self, attrs):
        if attrs.get('fecha_ini') > attrs.get('fecha_fin'):
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        return attrs


class RegistroHorarioSerializer(serializers.ModelSerializer):
    # Campo de llenado manual solamente requerido en la API REST
    observacion = serializers.CharField(required=True)
    class Meta:
        model = RegistroHorario


class RegistroHorarioRevisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegistroHorarioRevision


class FeriadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feriado


class UnidadOrganizacionalSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnidadOrganizacional


class DesignacionPermisoFuncionarioSerializer(serializers.ModelSerializer):
    # jefe = serializers.PrimaryKeyRelatedField(allow_null=True, source='jefe.username', queryset=User.objects.filter(is_superuser=True), required=False)
    # usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True)
    usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True)
    jefe = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(groups__name='JEFE DE UNIDAD'), required=True)
    # estado = serializers.CharField(read_only=True, default='creado')
    estado = serializers.ChoiceField(default='creado', choices=['creado', 'eliminado'])

    class Meta:
        model = DesignacionPermiso

    def create(self, validated_data):
        if validated_data['tipo_permiso'] == "olvido_marcar":
            validated_data['fecha_fin'] = validated_data['fecha_ini']
        return DesignacionPermiso.objects.create(**validated_data)


    # def create(self, validated_data):
    #     book = DesignacionPermiso.objects.create(title=validated_data['title'])
    #
    #     # Create or update each page instance
    #     for item in validated_data['pages']:
    #         page = Page(id=item['page_id'], text=item['text'], book=book)
    #         page.save()
    #
    #     return book

    def validate(self, attrs):
        # if attrs.get('tipo_permiso') == "olvido_marcar":
        #     print "holas"
        #     attrs['fecha_fin'] = attrs['fecha_ini']

        user_auth = self.context['request'].user

        if attrs.get('tipo_permiso') == "olvido_marcar" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la comisión'})

        if attrs.get('tipo_permiso') == "comision" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la comisión'})

        if attrs.get('tipo_permiso') == "comision" and not attrs.get('comision_lugar'):
            raise serializers.ValidationError({'Lugar': 'Debe ingresar el lugar donde se realizo la salida en comisión'})

        if attrs.get('tipo_permiso') == "comision":
            if not attrs.get('hora_ini') or not attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Debe ingresar las horas de salida y retorno de la comisión'})

        if attrs.get('tipo_permiso') == "particular":
            if not attrs.get('hora_ini') or not attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Debe ingresar las horas de salida y retorno del permiso particular'})

        # if attrs.get('tipo_permiso') == "particular" and not (attrs.get('particular_numeral_iii') or attrs.get('particular_numeral_iv')):
        #     raise serializers.ValidationError({'Numeral III y/o IV': 'Debe ingresar el motivo del permiso art. 31, numeral III o numeral VI'})

        if attrs.get('tipo_permiso') == "licencia" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la licencia'})

        if attrs.get('tipo_permiso') == "licencia" and not attrs.get('licencia_respaldo'):
            raise serializers.ValidationError({'Documento de respaldo': 'Debe ingresar el título/código del documento de respaldo'})

        if attrs.get('fecha_ini') > attrs.get('fecha_fin') and (attrs.get('tipo_permiso') in ["comision", "particular", "licencia"]):
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('fecha_ini') != attrs.get('fecha_fin') and attrs.get('tipo_permiso') == "olvido_marcar":
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser igual a la fecha final'})

        if attrs.get('hora_ini') and not attrs.get('hora_fin'):
            raise serializers.ValidationError({'Hora de salida y retorno': 'Si registra una hora de inicio debe colocar una hora final'})

        if attrs.get('hora_ini') and attrs.get('hora_fin'):
            if attrs.get('hora_ini') > attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Error la hora de inicio debe ser menor o igual a la hora final'})

        # if attrs.get('estado') != "creado":
        #     raise serializers.ValidationError({'Estado': 'Solo puede editar los registro que a creado y no estan aprobados aun'})

        if attrs.get('usuario') != user_auth:
            raise serializers.ValidationError({'Usuario': 'El usuario debe ser el mismo que se autentico en el sistema'})

        if attrs.get('tipo_permiso') == "comision" and not not_special_chars(attrs.get('motivo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "comision" and not not_special_chars(attrs.get('comision_lugar')):
            raise serializers.ValidationError({'Lugar': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "licencia" and not not_special_chars(attrs.get('motivo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "licencia" and not not_special_chars(attrs.get('licencia_respaldo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('usuario') == attrs.get('jefe'):
            raise serializers.ValidationError({'Error': 'El funcionario no puede solicitar el permiso a si mismo'})

        # if attrs.get('estado') == "creado":
        #     print user_auth
        #     raise serializers.ValidationError({'Motivo': 'Error Desconocido XD!!! carly'})
        return attrs


class DesignacionPermisoJefeSerializer(serializers.ModelSerializer):
    # estado = serializers.CharField(default='aprobado_jefe')
    estado = serializers.ChoiceField(default='aprobado_jefe', choices=['creado', 'aprobado_jefe', 'rechazado'])
    usuario = serializers.SerializerMethodField('custom_usuario')
    jefe = serializers.SerializerMethodField('custom_jefe')
    unidad_organizacional = serializers.SerializerMethodField('custom_unidad_organizacional')

    def custom_usuario(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_jefe(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_unidad_organizacional(self, xfield):
        return xfield.unidad_organizacional.unidad_organizacional

    # def validate(self, attrs):
    #     jefe_auth = self.context['request'].user
    #     print attrs.get('jefe')
    #     print jefe_auth.id
    #     if attrs.get('jefe') != jefe_auth:
    #         raise serializers.ValidationError({'Jefe': 'El jefe debe ser el mismo que se autentico en el sistema'})
    #
    #     return attrs

    class Meta:
        model = DesignacionPermiso
        read_only_fields = ('tipo_permiso', 'unidad_organizacional', 'motivo', 'comision_lugar', 'licencia_respaldo', 'particular_numeral_iii', 'particular_numeral_iv', 'fecha_ini', 'fecha_fin', 'hora_ini', 'hora_fin', 'fecha_registro', 'usuario', 'jefe',)


class DesignacionPermisoRrhhSerializer(serializers.ModelSerializer):
    estado = serializers.ChoiceField(default='aprobado_jefe', choices=['aprobado_jefe', 'aprobado_rrhh', 'rechazado'])
    usuario = serializers.SerializerMethodField('custom_usuario')
    jefe = serializers.SerializerMethodField('custom_jefe')
    unidad_organizacional = serializers.SerializerMethodField('custom_unidad_organizacional')

    def custom_usuario(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_jefe(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_unidad_organizacional(self, xfield):
        return xfield.unidad_organizacional.unidad_organizacional

    class Meta:
        model = DesignacionPermiso
        read_only_fields = ('tipo_permiso', 'unidad_organizacional', 'motivo', 'comision_lugar', 'licencia_respaldo', 'particular_numeral_iii', 'particular_numeral_iv', 'fecha_ini', 'fecha_fin', 'hora_ini', 'hora_fin', 'fecha_registro', 'usuario', 'jefe',)


# Seguridad para que el usuario solo edite sus registros
# Seguridad para que no puedan eliminar los datos con otra fecha y
class DesignacionPermisoSerializer(serializers.ModelSerializer):
    usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True)
    jefe = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(groups__name='JEFE DE UNIDAD'), required=True)

    class Meta:
        model = DesignacionPermiso

    def validate(self, attrs):
        if attrs.get('tipo_permiso') == "olvido_marcar" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la comisión'})

        if attrs.get('tipo_permiso') == "comision" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la comisión'})

        if attrs.get('tipo_permiso') == "comision" and not attrs.get('comision_lugar'):
            raise serializers.ValidationError({'Lugar': 'Debe ingresar el lugar donde se realizo la salida en comisión'})

        if attrs.get('tipo_permiso') == "comision":
            if not attrs.get('hora_ini') or not attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Debe ingresar las horas de salida y retorno de la comisión'})

        if attrs.get('tipo_permiso') == "particular":
            if not attrs.get('hora_ini') or not attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Debe ingresar las horas de salida y retorno del permiso particular'})

        if attrs.get('tipo_permiso') == "licencia" and not attrs.get('motivo'):
            raise serializers.ValidationError({'Motivo': 'Debe ingresar el motivo de la licencia'})

        if attrs.get('tipo_permiso') == "licencia" and not attrs.get('licencia_respaldo'):
            raise serializers.ValidationError({'Documento de respaldo': 'Debe ingresar el título/código del documento de respaldo'})

        if attrs.get('fecha_ini') > attrs.get('fecha_fin') and (attrs.get('tipo_permiso') in ["comision", "particular", "licencia"]):
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('fecha_ini') != attrs.get('fecha_fin') and attrs.get('tipo_permiso') == "olvido_marcar":
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser igual a la fecha final'})

        if attrs.get('hora_ini') and not attrs.get('hora_fin'):
            raise serializers.ValidationError({'Hora de salida y retorno': 'Si registra una hora de inicio debe colocar una hora final'})

        if attrs.get('hora_ini') and attrs.get('hora_fin'):
            if attrs.get('hora_ini') > attrs.get('hora_fin'):
                raise serializers.ValidationError({'Hora de salida y retorno': 'Error la hora de inicio debe ser menor o igual a la hora final'})

        if attrs.get('tipo_permiso') == "comision" and not not_special_chars(attrs.get('motivo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "comision" and not not_special_chars(attrs.get('comision_lugar')):
            raise serializers.ValidationError({'Lugar': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "licencia" and not not_special_chars(attrs.get('motivo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('tipo_permiso') == "licencia" and not not_special_chars(attrs.get('licencia_respaldo')):
            raise serializers.ValidationError({'Motivo': 'No se permiten carácteres especiales'})

        if attrs.get('usuario') == attrs.get('jefe'):
            raise serializers.ValidationError({'Error': 'El funcionario no puede solicitar el permiso a si mismo'})

        return attrs


class DesignacionPermisoHistoricoSerializer(serializers.ModelSerializer):
    jefe = serializers.CharField()
    unidad_organizacional = serializers.CharField()

    class Meta:
        model = DesignacionPermiso


class ConfiguracionGlobalSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfiguracionGlobal


# Aux proc ----------------------------------------------------------------------------
class AttrDict(dict):
     def __init__(self):
           self.__dict__ = self


class DesignacionVacacionFuncionarioSerializer(serializers.ModelSerializer):
    usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True)
    jefe = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(groups__name='JEFE DE UNIDAD'), required=True)
    estado = serializers.ChoiceField(default='creado', choices=['creado', 'eliminado'])

    class Meta:
        model = DesignacionVacacion

    def validate(self, attrs):
        user_auth = self.context['request'].user
        xitem = AttrDict()
        xitem.tipo_vacacion = attrs.get('tipo_vacacion')
        xitem.fecha_ini = attrs.get('fecha_ini')
        xitem.fecha_fin = attrs.get('fecha_fin')

        # DETECTA SI ES UNA INSTANCIA EXISTENTE PUT
        xput_id = None
        if self.instance:
            xput_id = self.instance.id
        # if self.context['request'].method in ['PUT', 'PATCH']:
        #     xitem = None

        # xitem.hora_ini = attrs.get('hora_ini')
        # xitem.hora_fin = attrs.get('hora_fin')

        tdias = DesignacionVacacion.objects.diasTotal(user_auth.id, xitem, xput_id)
        dias_total_cas = User.vacacion.casTotal(user_auth.id)

        # print user_auth.id
        if tdias > dias_total_cas and attrs.get('estado') != "eliminado":
            raise serializers.ValidationError({'vacacion': 'Usted se excedio con sus dias de vacacion permitidos'})

        if not attrs.get('fecha_ini') or not attrs.get('fecha_fin'):
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('fecha_ini') and attrs.get('fecha_fin'):
            if attrs.get('fecha_ini') > attrs.get('fecha_fin'):
                raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('usuario') == attrs.get('jefe'):
            raise serializers.ValidationError({'Error': 'El funcionario no puede solicitar el vacacion a si mismo'})

        return attrs

class DesignacionVacacionJefeSerializer(serializers.ModelSerializer):
    estado = serializers.ChoiceField(default='aprobado', choices=['creado', 'aprobado', 'rechazado'])
    usuario = serializers.SerializerMethodField('custom_usuario')
    jefe = serializers.SerializerMethodField('custom_jefe')
    unidad_organizacional = serializers.SerializerMethodField('custom_unidad_organizacional')
    username = serializers.SerializerMethodField('custom_username')

    def custom_username(self, xfield):
        return "%s" % (xfield.usuario.username)

    def custom_usuario(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_jefe(self, xfield):
        return "%s %s" % (xfield.usuario.first_name, xfield.usuario.last_name)

    def custom_unidad_organizacional(self, xfield):
        return xfield.unidad_organizacional.unidad_organizacional

    # def validate(self, attrs):
    #     jefe_auth = self.context['request'].user
    #     print attrs.get('jefe')
    #     print jefe_auth.id
    #     if attrs.get('jefe') != jefe_auth:
    #         raise serializers.ValidationError({'Jefe': 'El jefe debe ser el mismo que se autentico en el sistema'})
    #
    #     return attrs

    class Meta:
        model = DesignacionVacacion

        # fields = ('tipo_vacacion', 'unidad_organizacional', 'fecha_ini', 'fecha_fin', 'hora_ini', 'hora_fin', 'fecha_registro', 'usuario', 'jefe', 'usuario_group', 'estado',)
        read_only_fields = ('tipo_vacacion', 'unidad_organizacional', 'fecha_ini', 'fecha_fin', 'fecha_registro', 'usuario', 'jefe', 'username',)


class DesignacionVacacionSerializer(serializers.ModelSerializer):
    usuario = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(habilitado_marcar=True), required=True)
    jefe = serializers.PrimaryKeyRelatedField(allow_null=False, queryset=User.objects.filter(groups__name='JEFE DE UNIDAD'), required=True)

    class Meta:
        model = DesignacionVacacion

    def validate(self, attrs):
        user_auth = attrs.get('usuario')
        xitem = AttrDict()
        xitem.tipo_vacacion = attrs.get('tipo_vacacion')
        xitem.fecha_ini = attrs.get('fecha_ini')
        xitem.fecha_fin = attrs.get('fecha_fin')

        # DETECTA SI ES UNA INSTANCIA EXISTENTE PUT
        xput_id = None
        if self.instance:
            xput_id = self.instance.id

        tdias = DesignacionVacacion.objects.diasTotal(user_auth.id, xitem, xput_id)
        dias_total_cas = User.vacacion.casTotal(user_auth.id)

        # print user_auth.id
        if tdias > dias_total_cas and attrs.get('estado') != "eliminado":
            raise serializers.ValidationError({'vacacion': 'Usted se excedio con sus dias de vacacion permitidos'})

        if not attrs.get('fecha_ini') or not attrs.get('fecha_fin'):
            raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('fecha_ini') and attrs.get('fecha_fin'):
            if attrs.get('fecha_ini') > attrs.get('fecha_fin'):
                raise serializers.ValidationError({'Fecha inicial y final': 'Error la fecha de inicio debe ser menor o igual a la fecha final'})

        if attrs.get('usuario') == attrs.get('jefe'):
            raise serializers.ValidationError({'Error': 'El funcionario no puede solicitar el vacacion a si mismo'})

        return attrs


class FichaPersonalSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaPersonal


class FichaTrayectoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaTrayectoria


class FichaEstudiosUniversitariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaEstudiosUniversitarios


class FichaEstudiosEspecializacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaEstudiosEspecializacion


class FichaIdiomasSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaIdiomas


class FichaCursosRelacionadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaCursosRelacionados


class FichaConocimientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaConocimientos


class FichaExperienciaLaboralSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaExperienciaLaboral


class FichaPersonalHistoricoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FichaPersonalHistorico

    def validate(self, attrs):
        user_auth = self.context['request'].user

        if attrs.get('usuario') != user_auth:
            raise serializers.ValidationError({'Usuario': 'El usuario debe ser el mismo que se autentico en el sistema'})

        return attrs
