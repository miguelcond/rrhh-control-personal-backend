#!/usr/bin/python
# -*- coding: utf8 -*-
# from django.contrib.auth.decorators import login_required
from models import User, Group
# from django.shortcuts import render
# from django.http import HttpResponse
# from models import DesignacionPermiso
from django.utils import timezone
import json
# from django.core import signing
# from django.core.signing import TimestampSigner
# import services
from services import get_service_api_registros, get_service_api_feriados
from models import ConfiguracionGlobal, HorarioRango, RegistroHorario, RegistroHorarioRevision, Feriado
# from datetime import datetime
from rest_framework import serializers
from rest_framework import filters
from agetic_rrhh.permissions import HasGroupPermission
# from rest_framework_proxy.views import ProxyView
from djangorest_proxy.views import ProxyView
from django.contrib.sites.shortcuts import get_current_site
import requests
from rest_framework import status
from rest_framework.response import Response


def pruebas(request):
    # print ItemListProxy().get_datos()
    # print requests.get('./xdatos/')
    # print render_to_string()
    # print get_current_site(request).domain
    # print render_to_string(ItemListProxy.as_view(), '')
    # print ItemListProxy.as_view()(request, as_string=True)
    # print ren
    # sw_tolerancia_acumulada = False
    # if items:
    #     sw_tolerancia_acumulada = items.first().horario_rango.horario.sw_tolerancia_acumulada
    #
    # for item in items:
    #     if not item.hora_entrada == None and item.hora_entrada > item.horario_rango.hora_entrada:
    #         tardanza += datetime.combine(date.today(), item.hora_entrada) - datetime.combine(date.today(), item.horario_rango.hora_entrada)

    # for item in DesignacionPermiso.objects.all():
    #     tardanza = timedelta(0)
    #     if item.hora_ini and item.hora_fin:
    #         # print "%s - %s" % (item.hora_ini, item.hora_fin)
    #         tardanza = datetime.combine(date.today(), item.hora_fin) - datetime.combine(date.today(), item.hora_ini)
    #
    #     print tardanza


    return HttpResponse("Resultado de pruebas")

# ----------------------------------------------------------------------------------------------
# SINCRONIZACION
# ----------------------------------------------------------------------------------------------
# TENGO QUE HACER LA SINCRONIZACION DE FERIADOS
def revision_feriado(request):
    """
    ## Hacer una Revision de Feriados

    Se sincroniza con el microservicio de feriados
    """
    api_registros = get_service_api_feriados()
    for api_registro in api_registros:
        fecha, created = Feriado.objects.get_or_create(fecha=api_registro, defaults={'descripcion': api_registros[api_registro][0]['nombre']})

    return HttpResponse("Revision de Feriados Completado")


def revision_horario_sync(api_registros, max_fecha):
    # se le envia el ultimo registro de revision para que las fechas que nos devuelvan sean mayores que ese
    # api rest ordenado por nombre y por fecha
    cglobal = ConfiguracionGlobal.objects.all().first()
    print "->XXX<---------------------------------------------------------"
    for api_registro in api_registros:
        print "------------------------------------------------xxxxxxxxxxx"
        print api_registro
        api_usuario = User.objects.filter(username=api_registro['uid']).first()
        # Mientras exista el usuario en el sistema, se procedera a registrar su horario
        if api_usuario:
            api_fecha = datetime.strptime(api_registro['fecha'], '%Y-%m-%d')
            xdia = api_fecha.strftime('%w')

            if xdia in ['0', '1', '2', '3', '4', '5', '6']:
                xdia_name = {'0': 'domingo', '1': 'lunes', '2': 'martes', '3': 'miercoles', '4': 'jueves', '5': 'viernes', '6': 'sabado'}[xdia]
                horario_dia_list = HorarioRango.objects.filter(horario=getattr(cglobal, 'horario_%s' % xdia_name))
                # Si el usuario esta designado a un horario, entonces sacar el rango de horas para ese horario
                reg_horario = DesignacionHorario.objects.filter(usuario=api_usuario, fecha_ini__lte=api_fecha, fecha_fin__gte=api_fecha).first()
                # permisos = DesignacionPermiso.objects.filter(fecha_ini__lte=fecha, fecha_fin__gte=fecha, usuario__username=param_uid, estado='aprobado_rrhh')
                if reg_horario:
                    # Asignar en un dia especifico de la semana
                    # print reg_horario

                    if getattr(reg_horario, 'designar_%s' % xdia_name):
                        horario_dia_list = HorarioRango.objects.filter(horario=reg_horario.horario)

                for hrango in horario_dia_list:
                    xreg_entrada = {'hora': None, 'biometrico': ''}
                    xreg_salida = {'hora': None, 'biometrico': ''}
                    for registro in api_registro['registros']:
                        registro = {'hora':datetime.strptime(registro['hora'], '%H:%M:%S').time(), 'biometrico':registro['dispositivo']}
                        if hrango.rango_entrada_ini <= registro['hora'] and registro['hora'] <= hrango.rango_entrada_fin:
                            if xreg_entrada['hora'] == None:
                                xreg_entrada = registro
                            if registro['hora'] < xreg_entrada['hora'] and not cglobal.horario_max_entrada:
                                xreg_entrada = registro
                            if registro['hora'] > xreg_entrada['hora'] and cglobal.horario_max_entrada:
                                xreg_entrada = registro

                        if hrango.rango_salida_ini <= registro['hora'] and registro['hora'] <= hrango.rango_salida_fin:
                            if xreg_salida['hora'] == None:
                                xreg_salida = registro
                            if registro['hora'] < xreg_salida['hora'] and not cglobal.horario_max_salida:
                                xreg_salida = registro
                            if registro['hora'] > xreg_salida['hora'] and cglobal.horario_max_salida:
                                xreg_salida = registro

                    # print api_fecha.date()
                    # print max_fecha
                    max_fecha = max_fecha if max_fecha > api_fecha.date() else api_fecha.date()
                    registro_horario = RegistroHorario.objects.filter(fecha=api_fecha, usuario=api_usuario, horario_rango=hrango)

                    if registro_horario:
                        registro_horario.update(hora_entrada=xreg_entrada['hora'], hora_salida=xreg_salida['hora'], biometrico_entrada=xreg_entrada['biometrico'], biometrico_salida=xreg_salida['biometrico'])
                    else:
                        RegistroHorario.objects.get_or_create(hora_entrada=xreg_entrada['hora'], hora_salida=xreg_salida['hora'], fecha=api_fecha, usuario=api_usuario, biometrico_entrada=xreg_entrada['biometrico'], biometrico_salida=xreg_salida['biometrico'], horario_rango=hrango)
                        # RegistroHorario.objects.get_or_create(hora_entrada=xreg_entrada['hora'], hora_salida=xreg_salida['hora'], fecha=api_fecha, usuario=api_usuario, horario_rango=hrango)

    return max_fecha

# @login_required
def revision_horario(request):
    """
    ## Hacer una Revision de Horario

    Se comienza a realizar una revision de la tabla revision_horario distinguiendo cuales datos corresponden a entradas
    o salidas dependiendo del horario que esta designado al usuario y el rango de marcaciones
    """
    print "hola"
    param_fecha = request.GET.get('fecha')

    if param_fecha:
        max_fecha = datetime.strptime(param_fecha, '%d/%m/%Y').date()

    else:
        rev_horario = RegistroHorarioRevision.objects.order_by('-fecha').first()
        if rev_horario:
            max_fecha = rev_horario.fecha
        else:
            max_fecha = datetime.strptime('01/05/2016', '%d/%m/%Y').date()

    # RegistroHorario.objects.filter(fecha__gte=max_fecha).exclude(observacion__isnull=True).delete()
    # RegistroHorario.objects.filter(fecha__gte=max_fecha).exclude(observacion__isnull=True).delete()
    # FIXME modo destructivo, validar si es necesario más adelante
    RegistroHorario.objects.filter(fecha__gte=max_fecha).delete()
    api_registros = get_service_api_registros(max_fecha)
    max_fecha = revision_horario_sync(api_registros, max_fecha)

    # Guardando fecha de la ultima revision
    RegistroHorarioRevision.objects.get_or_create(fecha=max_fecha)

    return HttpResponse("Revision de Horario Completado")


from django.core import signing
from django.core.signing import TimestampSigner

#me falta un horario que dias controla en semana
#SE DEBE REALIZAR UNA PLANTILLA PARA NOTIFICACION RAPIDA
#Y UNA VEZ APROBADO EL USUARIO RECIEN PODRA IMPRIMIR EL PERMISO
# @login_required
def confirmar_permiso(request):
    """
    ## Confirma un permiso enviado por la url

    Estado=Aprobado
    http://127.0.0.1:8000/confirmar/permiso/?token=<TOKEN>&estado=1

    Estado=No Aprobado
    http://127.0.0.1:8000/confirmar/permiso/?token=<TOKEN>&estado=0

    La utilidad de esta liibreria es para otorgar un permiso via email
    """
    token_mail = request.GET.get("token")

    estado = True if request.GET.get("estado") == "1" else False
    print estado
    print token_mail
    signer = TimestampSigner(salt="4g3t1c")
    try:
        pk_dpermiso = signer.unsign(token_mail, max_age=3600*1)
        designacion_permiso = DesignacionPermiso.objects.filter(pk=pk_dpermiso, token_mail=token_mail, token_confirmation_date__isnull=True)
        if designacion_permiso:
            designacion_permiso.update(aprobado=estado, token_confirmation_date=timezone.now())
            return HttpResponse("El permiso fue aprobado satisfactoriamente")
        else:
            return HttpResponse("El permiso ya fue confirmado")
    except signing.SignatureExpired:
        return HttpResponse("La clave del mensaje a expirado")
    except signing.BadSignature:
        return HttpResponse("Firma de Mensaje Incorrecto")


# ----------------------------------------------------------------------------------------------
# LDAP USERS
# ----------------------------------------------------------------------------------------------
import ldap
from django.conf import settings


def home(request):
    group = Group.objects.get(name="FUNCIONARIO")
    respuesta = group.name

    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

    l = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
    l.simple_bind_s(settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD)
    users = l.search_ext_s(settings.X_LDAP_USERS, ldap.SCOPE_SUBTREE, settings.X_LDAP_CLASS, attrlist=["givenName", "sn", "mail", "title", "uid"])
    # users = l.search_ext_s(settings.X_LDAP_USERS, ldap.SCOPE_SUBTREE, "uid=usuario", attrlist=[settings.AUTH_LDAP_USER_ATTR_MAP[dx] for dx in settings.AUTH_LDAP_USER_ATTR_MAP])
    for user in users:
        print user[1][settings.X_LDAP_UID][0]

    return HttpResponse(respuesta)


# @login_required
def sincronizar_ldap(request):
    # l = ldap.initialize("ldap://192.168.20.252:389")
    # Parametros para menejar ldaps
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    # Conexion ldap find
    l = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
    l.simple_bind_s(settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD)
    # users = l.search_ext_s(settings.X_LDAP_USERS, ldap.SCOPE_SUBTREE, settings.X_LDAP_CLASS, attrlist=["givenName", "sn", "mail", "title", "uid"])
    # users = l.search_ext_s(settings.X_LDAP_USERS, ldap.SCOPE_SUBTREE, "uid=usuario", attrlist=[settings.AUTH_LDAP_USER_ATTR_MAP[dx] for dx in settings.AUTH_LDAP_USER_ATTR_MAP])

    users = l.search_ext_s(settings.X_LDAP_USERS, ldap.SCOPE_SUBTREE, settings.X_LDAP_CLASS, attrlist=[settings.AUTH_LDAP_USER_ATTR_MAP[dx] for dx in settings.AUTH_LDAP_USER_ATTR_MAP])
    for user in users:
        defaults = {}
        # obtener los atributos de ldap settings y pasarlo a su equivalente model user django
        for dx in settings.AUTH_LDAP_USER_ATTR_MAP:
            # si existe el atributo ldap en el arbol acceder a el
            if settings.AUTH_LDAP_USER_ATTR_MAP[dx] in user[1]:
                defaults[dx] = user[1][settings.AUTH_LDAP_USER_ATTR_MAP[dx]][0]

        usuario, created = User.objects.get_or_create(username=user[1][settings.X_LDAP_UID][0], defaults=defaults)
        if created:
            defaults['groups'] = Group.objects.get(name="FUNCIONARIO")
            grupo = Group.objects.get(name="FUNCIONARIO")
            usuario.groups.add(grupo)

    return HttpResponse("Syncronizacion completada")


# ----------------------------------------------------------------------------------------------
# JWToken DATA
# ----------------------------------------------------------------------------------------------
def jwt_response_payload_handler(token, user=None, request=None):
    org_menu = []
    if user.groups.all():
        org_menu = json.loads(user.groups.all()[0].menu)
    if 'control_personal.view_sugerencia' in list(user.get_all_permissions()):
        add_menu = {
            "label": "Sugerencias",
            "url" : "sugerencia",
            "icon": "comment"
        }
        org_menu.insert(1, add_menu)

    return {
        'token': token,
        'user': UserSerializer(user).data,
        'menu': json.loads(Group.objects.get(name="SUPERUSER").menu) if user.is_superuser else org_menu,
        'vacacion_enable': ConfiguracionGlobal.objects.all().first().form_enable_vacaciones
    }
# ----------------------------------------------------------------------------------------------
# API REST
# ----------------------------------------------------------------------------------------------
from models import Funcionario, Horario, HorarioRango, DesignacionHorario, RegistroHorario, Feriado, UnidadOrganizacional, DesignacionPermiso, ConfiguracionGlobal
from serializers import UserSerializer, HorarioSerializer, HorarioRangoSerializer, DesignacionHorarioSerializer, RegistroHorarioSerializer, RegistroHorarioRevisionSerializer, FeriadoSerializer, UnidadOrganizacionalSerializer, DesignacionPermisoSerializer, DesignacionPermisoHistoricoSerializer, ConfiguracionGlobalSerializer
from rest_framework import viewsets
# from permissions import HorarioPermission
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser, DjangoModelPermissions
from rest_framework import viewsets, mixins
from serializers import DesignacionPermisoFuncionarioSerializer, DesignacionPermisoJefeSerializer, DesignacionPermisoRrhhSerializer


# REST TABLES
# class AllUserViewSet(viewsets.ModelViewSet):
#     """
#     ## Todos los usuarios validos para marcacion
#     """
#     permission_classes = [IsAuthenticated]
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#     pagination_class = None
#
#     def get_queryset(self):
#         usuario = self.request.user
#         items = User.objects.all()
#         print User.objects.filter(groups__name='JEFE DE UNIDAD')
#         if usuario:
#             if not "RRHH" in usuario.groups.values_list('name', flat=True) and not usuario.is_superuser:
#                 items = User.objects.filter(username=usuario)
#
#         return items

class AllUserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## Todos los usuarios validos para marcacion
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH' ,]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = None

    # def get_queryset(self):
    #     usuario = self.request.user
    #     items = User.objects.all()
    #     print User.objects.filter(groups__name='JEFE DE UNIDAD')
    #     if usuario:
    #         if not "RRHH" in usuario.groups.values_list('name', flat=True) and not usuario.is_superuser:
    #             items = User.objects.filter(username=usuario)
    #
    #     return items


class AllJefeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## Todos los usuarios validos para marcacion
    """
    permission_classes = [IsAuthenticated]
    queryset = User.objects.filter(groups__name='JEFE DE UNIDAD')
    serializer_class = UserSerializer
    pagination_class = None

    # def get_queryset(self):
    #     usuario = self.request.user
    #     items = User.objects.all()
    #     print User.objects.filter(groups__name='JEFE DE UNIDAD')
    #     if usuario:
    #         if not "RRHH" in usuario.groups.values_list('name', flat=True) and not usuario.is_superuser:
    #             items = User.objects.filter(username=usuario)
    #
    #     return items


class UserViewSet(viewsets.ModelViewSet):
    """
    ## Usuarios del sistema
    Trabajar con usuarios del sistema, solo tiene acceso el administrador del sistema
    """
    permission_classes = [IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('username', )
    ordering_fields = '__all__'


class HorarioViewSet(viewsets.ModelViewSet):
    """
    ## Establecer un Horario en el sistema
    Se define un tipo horario que contendra un rango de horarios para que los funcionarios puedan marcar

    __sw_tolerancia_acumulada__ Se activa el modo de tolerancia acumulada

    __tolerancia__ La tolerancia es el tiempo en minutos que tiene un funcionario de tolerancia
    __sw_tolerancia_acumulada__ = True => Minutos de tolerancia acumulada en un dia
    __sw_tolerancia_acumulada__ = False => Minutos de tolerancia por periodo en un dia
    """
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('descripcion', 'observacion',)
    ordering_fields = '__all__'
    # permission_classes = (HorarioPermission,)

# algoritmo para llenar la tabla de marcados, hacer una tabla de marcados
class HorarioRangoViewSet(viewsets.ModelViewSet):
    """
    ## Establecer un Rango de Horas para un Horario
    Se define un rango de horarios para identificar cual de los marcados corresponden a la entrada o salida definida en un rango de horarios
    """
    queryset = HorarioRango.objects.all()
    serializer_class = HorarioRangoSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('horario__descripcion', 'descripcion',)
    ordering_fields = '__all__'

class DesignacionHorarioViewSet(viewsets.ModelViewSet):
    """
    ## Se designa un horario a un funcionario
    EL horario designado sera en un rango designado por una fecha de inicio y final
    """
    queryset = DesignacionHorario.objects.all()
    serializer_class = DesignacionHorarioSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('horario__descripcion', 'usuario__username',)
    ordering_fields = '__all__'

    def options(self, request, *args, **kwargs):
        if self.metadata_class is None:
            return self.http_method_not_allowed(request, *args, **kwargs)
        data = self.metadata_class().determine_metadata(request, self)
        if 'POST' in data['actions']:
            data['actions']['POST']['usuario']['multiple'] = True
        if 'PUT' in data['actions']:
            data['actions']['PUT']['usuario']['multiple'] = True
        if 'PATCH' in data['actions']:
            data['actions']['PATCH']['usuario']['multiple'] = True
        return Response(data, status=status.HTTP_200_OK)


# class RegistroHorarioFilter(filters.FilterSet):
#     class Meta:
#         model = RegistroHorario
#         fields = ['usuario__username', 'fecha']


class RegistroHorarioViewSet(viewsets.ModelViewSet):
    """
    ## En este objeto se registran todos los Horarios
    Esta tabla es donde se registran todos los marcados realizados en los biometricos se realiza un sincronizacion
    de base de datos mediante un cliente rest
    """
    queryset = RegistroHorario.objects.all()
    serializer_class = RegistroHorarioSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha',)
    # filter_class = RegistroHorarioFilter
    ordering_fields = '__all__'


class RegistroHorarioRevisionViewSet(viewsets.ModelViewSet):
    """
    ## En esta api se se muestran las revisiones de horario efectuadas a la fecha
    La fecha guardada haca sera desde donde se iniciara la busqueda de registros
    """
    queryset = RegistroHorarioRevision.objects.all()
    serializer_class = RegistroHorarioRevisionSerializer


class RegistroHorarioRevisionUltimoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## En esta api se se muestran la ultima revision de horario efectuada a la fecha
    Esta ultima fecha de revision sera mostrada cuando el funcionario de RRHH quiera realizar la sincronizacion de registros
    """
    queryset = RegistroHorarioRevision.objects.all().order_by('-id')[:1]
    serializer_class = RegistroHorarioRevisionSerializer

    # def get_queryset(self):
    #     # items = RegistroHorarioRevision.objects.filter(usuario=self.request.user).first
    #     item = RegistroHorarioRevision.objects.latest('id')
    #     return item


class FeriadoViewSet(viewsets.ModelViewSet):
    """
    ## En este objeto se aprueban los feriados
    Los feriados aprobados son antes sugeridos por el API REST de FERIADOS que entrega tentativamente todos los feriados del año
    """
    queryset = Feriado.objects.all()
    serializer_class = FeriadoSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('descripcion', '=fecha',)
    ordering_fields = '__all__'


class UnidadOrganizacionalViewSet(viewsets.ModelViewSet):
    """
    ## En esta tabla se designara un permiso y se aprobara el mismo
    Se designa un permiso y mediante un __token_mail__ se aprueba por el inmediato superior del funcionario, los datos del inmediato superior se
    consumen de una API REST de ORGANIGRAMA
    """
    queryset = UnidadOrganizacional.objects.all()
    serializer_class = UnidadOrganizacionalSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('unidad_organizacional',)
    ordering_fields = '__all__'


# http://192.168.20.228:8000/api/v2/designacion_permisos_funcionario/
class DesignacionPermisoFuncionarioViewSet(mixins.CreateModelMixin,
                                           mixins.RetrieveModelMixin,
                                           mixins.UpdateModelMixin,
                                           # mixins.DestroyModelMixin,
                                           mixins.ListModelMixin,
                                           viewsets.GenericViewSet):
    """
    ## En esta API se genera el permiso por el funcionario
    Existen 3 tipos de Permisos Licencia, Comision, Particular
    Cada funcionario puede crear su boleta de permiso
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = DesignacionPermiso.objects.all().exclude(estado="eliminado")
    serializer_class = DesignacionPermisoFuncionarioSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha_ini', '=fecha_fin',)
    ordering_fields = '__all__'

    def get_queryset(self):
        items = DesignacionPermiso.objects.filter(usuario=self.request.user)
        return items


class DesignacionPermisoJefeViewSet(mixins.CreateModelMixin,
                                    mixins.RetrieveModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    viewsets.GenericViewSet):
# class DesignacionPermisoJefeViewSet(viewsets.ModelViewSet):
    paginator = None
    permission_classes = [HasGroupPermission]
    required_groups = ['JEFE DE UNIDAD', ]
    queryset = DesignacionPermiso.objects.all()
    serializer_class = DesignacionPermisoJefeSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha_ini', '=fecha_fin',)
    ordering_fields = '__all__'

    def get_queryset(self):
        items = DesignacionPermiso.objects.filter(jefe=self.request.user, estado='creado')
        return items


class DesignacionPermisoRrhhViewSet(mixins.CreateModelMixin,
                                    mixins.RetrieveModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    viewsets.GenericViewSet):
    paginator = None
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]
    queryset = DesignacionPermiso.objects.all()
    serializer_class = DesignacionPermisoRrhhSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha_ini', '=fecha_fin',)
    ordering_fields = '__all__'

    def get_queryset(self):
        items = DesignacionPermiso.objects.filter(estado='aprobado_jefe')
        return items


    # serializer_class = DesignacionPermisoSerializer
    # filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    # search_fields = ('tipo_permiso', 'usuario__username')
    # ordering_fields = '__all__'
    #
    # def get_queryset(self):
    # def get_queryset(self):
    #     autor = self.request.user
    #     items = DesignacionPermiso.objects.all()
    #     if autor:
    #         if not "RRHH" in autor.groups.values_list('name', flat=True):
    #             inicio_mes = datetime(timezone.now().year, timezone.now().month, 1, tzinfo=timezone.get_current_timezone())
    #             # items = DesignacionPermiso.objects.filter(autor=self.request.user, aprobado=False, fecha_registro__gte=inicio_mes)
    #             items = DesignacionPermiso.objects.filter(autor=self.request.user, fecha_registro__gte=inicio_mes)
    #
    #     return items




# class DesignacionPermisoViewSet(viewsets.ModelViewSet):
class DesignacionPermisoViewSet(mixins.CreateModelMixin,
                                mixins.RetrieveModelMixin,
                                mixins.UpdateModelMixin,
                                # mixins.DestroyModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    """
    ## En esta API se designara un permiso y se aprobara el mismo
    Existen 3 tipos de Permisos Licencia, Comision, Particular
    Solamente cada responsable de área o jefe de unidad pueden llenar las boletas
    Los responsables/jefes de unidad se tienen que autenticar mediante LDAP
    Habilitar roles para los responsables/jefes de unidad para llenar boletas
    """

    queryset = DesignacionPermiso.objects.all().order_by('-fecha_registro')
    serializer_class = DesignacionPermisoSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('tipo_permiso', 'usuario__username', 'usuario__first_name', 'usuario__last_name','comision_lugar', 'motivo',)
    ordering_fields = '__all__'

    def options(self, request, *args, **kwargs):
        if self.metadata_class is None:
            return self.http_method_not_allowed(request, *args, **kwargs)
        data = self.metadata_class().determine_metadata(request, self)
        if 'POST' in data['actions']:
            data['actions']['POST']['id']['label'] = "Correlativo"
        if 'PUT' in data['actions']:
            data['actions']['PUT']['id']['label'] = "Correlativo"
        if 'PATCH' in data['actions']:
            data['actions']['PATCH']['id']['label'] = "Correlativo"
        return Response(data, status=status.HTTP_200_OK)

    # Rol de usuario que crea las boletas, solamente puede ver sus boletas creadas y puede editar las boletas que creó hasta fin de mes
    # queryset = DesignacionPermiso.objects.filter(aprobado=False)

    # def get_queryset(self):
    #     autor = self.request.user
    #     items = DesignacionPermiso.objects.all()
    #     if autor:
    #         if not "RRHH" in autor.groups.values_list('name', flat=True):
    #             inicio_mes = datetime(timezone.now().year, timezone.now().month, 1, tzinfo=timezone.get_current_timezone())
    #             # items = DesignacionPermiso.objects.filter(autor=self.request.user, aprobado=False, fecha_registro__gte=inicio_mes)
    #             items = DesignacionPermiso.objects.filter(autor=self.request.user, fecha_registro__gte=inicio_mes)
    #
    #     return items


# Hacer un historico
# class HistoricoPermisoViewSet(viewsets.ModelViewSet):
# No utilizado
class HistoricoPermisoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## En esta API se muestra el historico de permisos del Autor
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]
    queryset = DesignacionPermiso.objects.all()
    serializer_class = DesignacionPermisoHistoricoSerializer
    # Rol de usuario que crea las boletas, solamente puede ver sus boletas creadas y puede editar las boletas que creó hasta fin de mes

    def get_queryset(self):
        param_fecha_ini = self.request.GET.get('fecha_ini')
        param_fecha_fin = self.request.GET.get('fecha_fin')
        if param_fecha_ini and param_fecha_fin:
            current_tz = timezone.get_current_timezone()
            fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y').replace(tzinfo=current_tz)
            fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y').replace(tzinfo=current_tz) + timedelta(days=1)

        if not fecha_ini or not fecha_fin:
            raise serializers.ValidationError("Debe ingresar una fecha de inicio y fin")

        if fecha_ini > fecha_fin:
            raise serializers.ValidationError("Error la fecha de inicio debe ser menor o igual a la fecha final")

        return DesignacionPermiso.objects.filter(autor=self.request.user, fecha_registro__gte=fecha_ini, fecha_registro__lte=fecha_fin)


# Hacer un historico para Usuarios
class HistoricoPermisoByUserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## En esta API se muestra el historico de permisos por Usuario Beneficiarion del Permiso
    """
    paginator = None
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', 'FUNCIONARIO', 'JEFE DE UNIDAD', ]
    queryset = DesignacionPermiso.objects.all().exclude(estado='eliminado').order_by('-fecha_registro')
    serializer_class = DesignacionPermisoHistoricoSerializer

    def get_queryset(self):
        param_fecha_ini = self.request.GET.get('fecha_ini')
        param_fecha_fin = self.request.GET.get('fecha_fin')
        if param_fecha_ini and param_fecha_fin:
            current_tz = timezone.get_current_timezone()
            fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y').replace(tzinfo=current_tz)
            fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y').replace(tzinfo=current_tz) + timedelta(days=1)

        if not fecha_ini or not fecha_fin:
            raise serializers.ValidationError("Debe ingresar una fecha de inicio y fin")

        if fecha_ini > fecha_fin:
            raise serializers.ValidationError("Error la fecha de inicio debe ser menor o igual a la fecha final")

        # return DesignacionPermiso.objects.filter(usuario=self.request.user, fecha_registro__gte=fecha_ini, fecha_registro__lte=fecha_fin).exclude(estado='eliminado').order_by('-fecha_registro')
        # TODO: Revisar esta parte si en produccion esta con esto o no
        return DesignacionPermiso.objects.filter(usuario=self.request.user, fecha_ini__gte=fecha_ini, fecha_fin__lte=fecha_fin).exclude(estado='eliminado').order_by('-fecha_registro')


class HistoricoPermisoUsersViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ## En esta API se muestra Todo el historico de permisos
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]
    queryset = DesignacionPermiso.objects.all()
    serializer_class = DesignacionPermisoSerializer

    def get_queryset(self):
        param_fecha_ini = self.request.GET.get('fecha_ini')
        param_fecha_fin = self.request.GET.get('fecha_fin')
        if param_fecha_ini and param_fecha_fin:
            current_tz = timezone.get_current_timezone()
            fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y').replace(tzinfo=current_tz)
            fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y').replace(tzinfo=current_tz) + timedelta(days=1)

        if not fecha_ini or not fecha_fin:
            raise serializers.ValidationError("Debe ingresar una fecha de inicio y fin")

        if fecha_ini > fecha_fin:
            raise serializers.ValidationError("Error la fecha de inicio debe ser menor o igual a la fecha final")

        return DesignacionPermiso.objects.filter(fecha_registro__gte=fecha_ini, fecha_registro__lte=fecha_fin)


class ConfiguracionGlobalViewSet(viewsets.ModelViewSet):
    """
    ## Configuracion de horarios por defecto
    marcacion en valores minimos o maximos y horarios por defecto
    """
    queryset = ConfiguracionGlobal.objects.all()
    serializer_class = ConfiguracionGlobalSerializer

    def options(self, request, *args, **kwargs):
        if self.metadata_class is None:
            return self.http_method_not_allowed(request, *args, **kwargs)
        data = self.metadata_class().determine_metadata(request, self)
        if 'POST' in data['actions']:
            data['actions']['POST']['form_enable_vacaciones']['type'] = "field"
            data['actions']['POST']['form_enable_vacaciones']['multiple'] = True
        if 'PUT' in data['actions']:
            data['actions']['PUT']['form_enable_vacaciones']['multiple'] = True
        if 'PATCH' in data['actions']:
            data['actions']['PATCH']['form_enable_vacaciones']['multiple'] = True
        return Response(data, status=status.HTTP_200_OK)


from models import Sugerencia
from serializers import SugerenciaSerializer


class SugerenciaPostViewSet(mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    """
    ## Configuracion de Sugerencias
    Registro de Sugerencias post desde el WebSite para la AGETIC
    """
    queryset = Sugerencia.objects.all()
    serializer_class = SugerenciaSerializer
    permission_classes = [AllowAny]


class SugerenciaViewSet(viewsets.ModelViewSet):
    """
    ## Configuracion de Sugerencias
    Registro de Sugerencias post desde el administrador para la AGETIC
    """
    # Por defecto utilizara DjangoModelGuardianPermission
    queryset = Sugerencia.objects.all().order_by('-fecha_registro')
    serializer_class = SugerenciaSerializer
    # permission_classes = [DjangoModelPermissions]
    # permission_classes = [HasGroupPermission, DjangoModelPermissions]
    # required_groups = ['RRHH', ]
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('sugerencia',)
    ordering_fields = '__all__'


from models import DesignacionVacacion
from serializers import DesignacionVacacionFuncionarioSerializer, DesignacionVacacionJefeSerializer, DesignacionVacacionSerializer


class DesignacionVacacionFuncionarioViewSet(mixins.CreateModelMixin,
                                           mixins.RetrieveModelMixin,
                                           mixins.UpdateModelMixin,
                                           # mixins.DestroyModelMixin,
                                           mixins.ListModelMixin,
                                           viewsets.GenericViewSet):
    """
    ## Registrar vacacion por el funcionario
    Existen 3 tipos: Todo el dia, medio dia, horas
    Cada funcionario puede registrar su vacacion para luego ser validado por su jefe
    """
    paginator = None
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = DesignacionVacacion.objects.all().exclude(estado="eliminado")
    serializer_class = DesignacionVacacionFuncionarioSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha_ini', '=fecha_fin',)
    ordering_fields = '__all__'

    def get_queryset(self):
	year_calendar = ConfiguracionGlobal.objects.yearActivo()
	# fecha_ini__year=year_calendar, fecha_fin__year=year_calendar
        items = DesignacionVacacion.objects.filter(usuario=self.request.user, fecha_ini__year=year_calendar, fecha_fin__year=year_calendar).exclude(estado="eliminado")
        return items


class DesignacionVacacionJefeViewSet(mixins.CreateModelMixin,
                                    mixins.RetrieveModelMixin,
                                    mixins.UpdateModelMixin,
                                    mixins.ListModelMixin,
                                    viewsets.GenericViewSet):
# class DesignacionPermisoJefeViewSet(viewsets.ModelViewSet):
    """
    ## Aprobar la vacacion del funcionario
    """
    paginator = None
    permission_classes = [HasGroupPermission]
    required_groups = ['JEFE DE UNIDAD', ]
    queryset = DesignacionVacacion.objects.all()
    serializer_class = DesignacionVacacionJefeSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username', '=fecha_ini', '=fecha_fin',)
    ordering_fields = '__all__'

    def get_queryset(self):
	year_calendar = ConfiguracionGlobal.objects.yearActivo()
	# fecha_ini__year=year_calendar, fecha_fin__year=year_calendar
        items = DesignacionVacacion.objects.filter(jefe=self.request.user, fecha_ini__year=year_calendar, fecha_fin__year=year_calendar, estado='creado')
        return items


class DesignacionVacacionViewSet(mixins.CreateModelMixin,
                                mixins.RetrieveModelMixin,
                                mixins.UpdateModelMixin,
                                # mixins.DestroyModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    """
    ## En esta API se designara una vacacion y se aprobara el mismo
    Existen 2 tipos de vacaciones por dias y medios dias
    Solamente cada responsable de área o jefe de unidad pueden aprobar las vacaciones
    Los responsables/jefes de unidad se tienen que autenticar mediante LDAP
    """

    queryset = DesignacionVacacion.objects.all().order_by('-fecha_registro')
    serializer_class = DesignacionVacacionSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('tipo_vacacion', 'usuario__username', 'usuario__first_name', 'usuario__last_name',)
    ordering_fields = '__all__'


# class HistoricoVacacionDependientesViewSet(viewsets.ReadOnlyModelViewSet):
from django.core import serializers
from django.db.models import Q


def HistoricoVacacionDependientes(request):
    """
    ## Listar la vacacion del funcionario
    """
    user_jwt = PermissionViewJWT(request).is_authenticated()
    param_year = request.GET.get('year', 1800)
    param_month = request.GET.get('month', 0)
    # year_calendar = ConfiguracionGlobal.objects.yearActivo()
    # fecha_ini__year=year_calendar, fecha_fin__year=year_calendar
    items = DesignacionVacacion.objects.filter(jefe=user_jwt.id, estado__in=['creado', 'aprobado'],fecha_ini__year=param_year, fecha_fin__year=param_year).filter(Q(fecha_ini__month=param_month) | Q(fecha_fin__month=param_month))
    xitem = []
    for item in items:
	xitem_row = {}
	xitem_row['fecha_ini'] = str(item.fecha_ini)
	xitem_row['fecha_fin'] = str(item.fecha_fin)
	xitem_row['usuario'] = item.usuario.first_name + " " + item.usuario.last_name
	xitem_row['tipo_vacacion'] = item.tipo_vacacion
	xitem_row['estado'] = item.estado
	xitem.append(xitem_row)
    # return JsonResponse(json.dumps(xitem), status=200, safe=False)
    return HttpResponse(json.dumps(xitem), content_type="application/json")


def DiasLaborales(request):
    cglobal = ConfiguracionGlobal.objects.all().first()
    xdia_name = {'6': 'domingo', '0': 'lunes', '1': 'martes', '2': 'miercoles', '3': 'jueves', '4': 'viernes', '5': 'sabado'}
    data = {}
    for item in xdia_name:
        data[xdia_name[item]] = True if getattr(cglobal, 'horario_%s' % xdia_name[item]) else False
    # data
    return HttpResponse(json.dumps(data), content_type="application/json")


def DiasVacacionRestantes(request):
    user_jwt = PermissionViewJWT(request).is_authenticated()
    tdias = DesignacionVacacion.objects.diasTotal(user_jwt.id)
    dias_total_cas = User.vacacion.casTotal(user_jwt.id)

    if tdias > dias_total_cas:
        return JsonResponse({'vacacion':'Dias restantes de vacacion agotado'}, status=400)

    return JsonResponse({'vacacion':dias_total_cas - tdias}, status=200)

 # {"status":200,"feriados":[{"fecha":"2017-01-02","nombre":"Año Nuevo"},{"fecha":"2017-01-23","nombre"
# :"Dia del Estado Plurinacional"}]}<Paste>


# def CalendarioDependientesVacaciones(request):
#     user_jwt = PermissionViewJWT(request).is_authenticated()
#     items = DesignacionVacacion.objects.filter(jefe__id=user_jwt.id)
#     dias_total_cas = User.vacacion.casTotal(user_jwt.id)

#     if tdias > dias_total_cas:
#         return JsonResponse({'vacacion':'Dias restantes de vacacion agotado'}, status=400)

#     return JsonResponse({'vacacion':dias_total_cas - tdias}, status=200)

# FICHA PERSONAL --------------------------------------------------------------------------------
from models import FichaPersonal, FichaTrayectoria, FichaEstudiosUniversitarios, FichaEstudiosEspecializacion, FichaCursosRelacionados, FichaIdiomas, FichaIdiomas, FichaConocimientos, FichaExperienciaLaboral, FichaPersonalHistorico
from serializers import FichaPersonalSerializer, FichaTrayectoriaSerializer, FichaEstudiosUniversitariosSerializer, FichaEstudiosEspecializacionSerializer, FichaCursosRelacionadosSerializer, FichaIdiomasSerializer, FichaIdiomasSerializer, FichaConocimientosSerializer, FichaExperienciaLaboralSerializer, FichaPersonalHistoricoSerializer

class FichaPersonalViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           # mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha personal de datos
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaPersonal.objects.all()
    serializer_class = FichaPersonalSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('usuario__username',)
    ordering_fields = '__all__'


class FichaTrayectoriaViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha personal trayectoria laboral en la entidad
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaTrayectoria.objects.all()
    serializer_class = FichaTrayectoriaSerializer


class FichaEstudiosUniversitariosViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
        ## Ficha estudios universitarios
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaEstudiosUniversitarios.objects.all()
    serializer_class = FichaEstudiosUniversitariosSerializer


class FichaEstudiosEspecializacionViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha estudios de especializacion
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaEstudiosEspecializacion.objects.all()
    serializer_class = FichaEstudiosEspecializacionSerializer


class FichaCursosRelacionadosViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha cursos relacionados con el area de trabajo
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaCursosRelacionados.objects.all()
    serializer_class = FichaCursosRelacionadosSerializer


class FichaIdiomasViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha idiomas
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaIdiomas.objects.all()
    serializer_class = FichaIdiomasSerializer


class FichaConocimientosViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha otros conocimientos (paquetes de computacion)
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaConocimientos.objects.all()
    serializer_class = FichaConocimientosSerializer


class FichaExperienciaLaboralViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha experiencia laboral
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaExperienciaLaboral.objects.all()
    serializer_class = FichaExperienciaLaboralSerializer


class FichaPersonalHistoricoViewSet(mixins.CreateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           # mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    ## Ficha personal de datos
    """
    permission_classes = [HasGroupPermission]
    required_groups = ['FUNCIONARIO', 'JEFE DE UNIDAD', 'RRHH']
    queryset = FichaPersonalHistorico.objects.all()
    serializer_class = FichaPersonalHistoricoSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    # search_fields = ('usuario__username',)
    ordering_fields = '__all__'

    def get_queryset(self):
        items = FichaPersonalHistorico.objects.filter(usuario=self.request.user)
        return items

# ----------------------------------------------------------------------------------------------
# BIOMETRICO PROXY
# ----------------------------------------------------------------------------------------------
# from rest_framework_proxy.views import ProxyView
# from rest_framework.permissions import IsAuthenticated, AllowAny


class BiometricoProxy(ProxyView):
    proxy_key = 'BIOMETRICOS'
    source = 'api/v1/biometricos'
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]


class RegistrosProxy(ProxyView):
    proxy_key = 'BIOMETRICOS'
    source = 'api/v1/registros'
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]


class RegistrosUsuarioProxy(ProxyView):
    proxy_key = 'BIOMETRICOS'
    source = 'api/v1/registros_usuario'
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]


class RegistrosUsuarioDiaProxy(ProxyView):
    proxy_key = 'BIOMETRICOS'
    source = 'api/v1/registros_usuario_dia'
    permission_classes = [IsAuthenticated]
    # required_groups = ['RRHH', ]


class RegistrosRangoProxy(ProxyView):
    proxy_key = 'BIOMETRICOS'
    source = 'api/v1/registros_rango'
    permission_classes = [HasGroupPermission]
    required_groups = ['RRHH', ]


class FeriadosProxy(ProxyView):
    proxy_key = 'FERIADOS'
    source = 'v1/feriados'
    permission_classes = [IsAuthenticated]
    # permission_classes = (IsAuthenticated,)

# ----------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------
# from rest_framework.request import Request
# from django.utils.functional import SimpleLazyObject
# from django.contrib.auth.middleware import get_user
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication
#
# def get_user_jwt(request):
#     user = get_user(request)
#     if user.is_authenticated():
#         return user
#     try:
#         user_jwt = JSONWebTokenAuthentication().authenticate(Request(request))
#         if user_jwt is not None:
#             return user_jwt[0]
#     except:
#         pass
#     return user
#
#
# class AuthenticationMiddlewareJWT(object):
#     def process_request(self, request):
#         assert hasattr(request, 'session'), "The Django authentication middleware requires session middleware to be installed. Edit your MIDDLEWARE_CLASSES setting to insert 'django.contrib.sessions.middleware.SessionMiddleware'."
#
#         request.user = SimpleLazyObject(lambda: get_user_jwt(request))
# ----------------------------------------------------------------------------------------------
#administrador
#auditos solo lectura todos
#recursos humanos
# ----------------------------------------------------------------------------------------------
# JWT PERMISSION TO VIEW
# ----------------------------------------------------------------------------------------------
from models import Feriado
# from datetime import timedelta, date
import pyexcel.ext.xls
import pyexcel.ext.ods3
import django_excel as excel
from datetime import datetime, date, timedelta
from django.utils.dates import WEEKDAYS_ABBR
from django.contrib.auth.middleware import get_user
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.request import Request
from django.core.exceptions import ValidationError
from rest_framework_jwt import utils as utils_drf


class PermissionViewJWT:
    def __init__(self, xrequest):
        self.user = self.get_user_jwt(xrequest)

    def is_authenticated(self):

        if not self.user.is_authenticated():
            raise ValidationError('Debe autenticarse para ingresar a este recurso')

        return self.user

    def permission_required_groups(self, required_groups):
        if self.is_authenticated():
            if self.user.is_superuser:
                return self.user

            user_groups = self.user.groups.values_list('name', flat=True)
            if not set(required_groups).intersection(user_groups):
                raise ValidationError('No tiene los permisos para acceder a este recurso')

            return self.user

    def get_user_jwt(self, xrequest):
        user = get_user(xrequest)
        if user.is_authenticated():
            return user
        try:
            url_token = xrequest.GET.get('s')
            if url_token:
                decoded_payload = utils_drf.jwt_decode_handler(url_token)
                if decoded_payload:
                    return User.objects.get(username=decoded_payload['username'])

            user_jwt = JSONWebTokenAuthentication().authenticate(Request(xrequest))
            if user_jwt is not None:
                return user_jwt[0]
        except:
            pass
        return user


# ----------------------------------------------------------------------------------------------
# REPORT CSV
# ----------------------------------------------------------------------------------------------
def strfdelta(tdelta):
    if tdelta:
        d = {"days": tdelta.days}
        d["hours"], rem = divmod(tdelta.seconds, 3600)
        d["minutes"], d["seconds"] = divmod(rem, 60)

        tiempo_str = "%s:%s" % (str(d["hours"]).zfill(2), str(d["minutes"]).zfill(2))
        if tdelta.days:
            tiempo_str = "%s dia, %s" % (tdelta.days, tiempo_str)

        if tiempo_str == '00:00':
            tiempo_str = ''
        return tiempo_str
    else:
        return ''


def strfdelta_org(tdelta, fmt):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    if tdelta.days:
        d["horas"] = "%s dia, %s" % (tdelta.days, str(d["hours"]).zfill(2))
    else:
        d["horas"] = str(d["hours"]).zfill(2)
    d["minutos"] = str(d["minutes"]).zfill(2)
    d["segundos"] = str(d["seconds"]).zfill(2)
    return fmt.format(**d)


def render_time(tiempo):
    return tiempo.strftime('%H:%M') if tiempo else ''


def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


def marcaciones_parse_horas(cglobal, xdia_name, fecha, items, feriado, dia_semana, permisos, param_tipo, usuario):
    data = []
    if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]):
        # print ">> %s <<" % dia_semana
        if items:
            for item in items:
                # print "--------------------------------------------"
                # print item.id
                # print item.fecha
                # print item.horario_rango

                # if item.hora_entrada:
                #     print "---------------------------------------------------"
                #     print item.hora_entrada
                #     print timedelta(minutes=1)
                #     print datetime.combine(date.today(), item.hora_entrada)
                #     print datetime.combine(date.today(), item.hora_entrada) - timedelta(minutes=1)

                tolerancia = item.horario_rango.horario.tolerancia

                tardanza = ''
                if not item.hora_entrada == None and item.hora_entrada > item.horario_rango.hora_entrada:
                    hora_entrada_tolerancia = datetime.combine(date.today(), item.hora_entrada) - timedelta(minutes=tolerancia)
                    if not permisos.filter(hora_ini__lte=item.hora_entrada, hora_fin__gte=hora_entrada_tolerancia):
                        permiso_tolerancia_entrada = permisos.filter(hora_ini__lte=item.horario_rango.hora_entrada, hora_fin__gte=item.horario_rango.hora_entrada)
                        v_hora_entrada = item.horario_rango.hora_entrada
                        if permiso_tolerancia_entrada:
                            v_hora_entrada = permiso_tolerancia_entrada.first().hora_fin
                        tardanza = datetime.combine(date.today(), item.hora_entrada) - datetime.combine(date.today(), v_hora_entrada)

                temprano = ''
                if not item.hora_salida == None and item.hora_salida < item.horario_rango.hora_salida:
                    hora_salida_tolerancia = datetime.combine(date.today(), item.hora_salida) - timedelta(minutes=tolerancia)
                    if not permisos.filter(hora_ini__lte=item.hora_salida, hora_fin__gte=hora_salida_tolerancia):
                        temprano = datetime.combine(date.today(), item.horario_rango.hora_salida) - datetime.combine(date.today(), item.hora_salida)


                # print "---------------------------"
                # print item.hora_entrada
                # print item.horario_rango.hora_entrada
                # print "---------------------------"


                # sw_tolerancia_acumulada = False
                #  tolerancia = timedelta(0)
                # if items:
                #     tolerancia = timedelta(minutes=items.first().horario_rango.horario.tolerancia)
                #     sw_tolerancia_acumulada = items.first().horario_rango.horario.sw_tolerancia_acumulada
                # from operator import xor
                # tardanza_time = datetime.strptime(tardanza, '%H:%M:%S').time()
                # print strfdelta(tardanza, "{days} days {hours}:{minutes}:{seconds}")
                # tardanza_str = strfdelta(tardanza)
                # temprano_str = strfdelta(temprano)

                sw_tolerancia_acumulada = item.horario_rango.horario.sw_tolerancia_acumulada
                if tardanza:
                    if tardanza - timedelta(minutes=tolerancia) <= timedelta(0):
                        tardanza = ''
                    else:
                        tardanza = tardanza - timedelta(minutes=tolerancia)

                # print "---------------------------"
                # print permisos.filter(hora_ini__lte=item.hora_entrada, hora_fin__gte=item.hora_entrada)
                # print permisos.hora_fin
                # print "---------------------------"

                # Delimitar el intervalo en el cual afecta el permiso
                xpermisos = permisos.filter(hora_ini__range=[item.horario_rango.rango_entrada_ini, item.horario_rango.rango_salida_fin]) or permisos.filter(hora_fin__range=[item.horario_rango.rango_entrada_ini, item.horario_rango.rango_salida_fin])

                data_body = [
                    item.usuario.ci if item.usuario.ci else '',
                    item.usuario.username,
                    ('%s %s')%(item.usuario.first_name, item.usuario.last_name),
                    dia_semana,
                    item.fecha.strftime('%d/%m/%Y'),
                    # item.horario_rango.descripcion,
                    # Si es feriado mostrar el titulo del feriado y marcar con X  if-> (Verficar si es dia Habil de Marcacion)
                    (feriado.descripcion if feriado else ('CON PERMISO' if xpermisos else item.horario_rango.descripcion)) if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]) else 'SIN HORARIO',
                    'X' if feriado else '',

                    render_time(item.horario_rango.hora_entrada),
                    render_time(item.horario_rango.hora_salida),
                    render_time(item.hora_entrada),
                    render_time(item.hora_salida),
                    item.biometrico_entrada,
                    item.biometrico_salida,
                    # datetime.strptime(registro['hora'], '%H:%M:%S').time()

                    # '' if permisos else str(tardanza.strftime('%H:%M')),
                    # '' if permisos else str(tardanza_str),
                    # '' if permisos else str(temprano_str),

                    # '' if permisos else tardanza,
                    # '' if permisos else temprano,
                    tardanza,
                    temprano,

                    # 'Falta 2222' if item.biometrico_entrada or item.biometrico_salida else 'Falto'
                    #('CON PERMISO' if permisos else 'FALTA') if not (item.biometrico_entrada and item.biometrico_salida) else ''
                    # 'CON PERMISO' if xpermisos else ('FALTA' if not (item.biometrico_entrada and item.biometrico_salida) else '')
                    'CON PERMISO' if xpermisos else ('FALTA' if not (item.hora_entrada and item.hora_salida) else '')
                ]
                if param_tipo == 'json':
                    tolerancia = item.horario_rango.horario.tolerancia
                    sw_tolerancia_acumulada = item.horario_rango.horario.sw_tolerancia_acumulada
                    # TENGO QUE APLICAR EL CALCULO DE TOLERANCIA AQUI

                    # sw_tolerancia_acumulada = item.horario_rango.horario.sw_tolerancia_acumulada
                    # if not sw_tolerancia_acumulada:
                    #     tolerancia = item.horario_rango.tolerancia_individual
                    #

                    # data_body.pop(0)
                    data_body.append(tolerancia)
                    data_body.append(sw_tolerancia_acumulada)

                data.append(data_body)

        else:
            # print "******************************-|-> %s <-|-**************************" % items
            data_body = [
                usuario.ci if usuario.ci else '',
                usuario.username,
                ('%s %s')%(usuario.first_name, usuario.last_name),
                dia_semana,
                fecha.strftime('%d/%m/%Y'),
                # 'FALTO',
                # (Si es feriado mostrar el titulo del feriado y marcar con X) if-> (Verficar si es dia Habil de Marcacion)
                (feriado.descripcion if feriado else ('CON PERMISO' if permisos else 'FALTO')) if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]) else 'SIN HORARIO',
                'X' if feriado else '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '' if feriado else ('CON PERMISO' if permisos else 'FALTA')
            ]
            if param_tipo == 'json':
                # data_body.pop(0)
                data_body.append(0)
                data_body.append(False)

            data.append(data_body)
        # print param_uid
    return data


def reporte_marcaciones(request, individual):
    # Si el reporte es individual o es para todos emitido por la gente de RRHH
    param_tipo = request.GET.get('tipo')
    if param_tipo == 'json':
        if individual:
            user_jwt = PermissionViewJWT(request).is_authenticated()
            param_uid = user_jwt
        else:
            PermissionViewJWT(request).permission_required_groups(['RRHH', ])
            param_uid = request.GET.get('uid')
    else:
        param_uid = request.GET.get('uid')

    # print param_uid
    param_fecha_ini = request.GET.get('fecha_ini')
    param_fecha_fin = request.GET.get('fecha_fin')


    data = []
    data_head = ['CI', 'UID', 'Nombre', '', 'Fecha', 'Horario', 'Feriado', 'Hora-Ent', 'Hora-Sal', 'Marc-Ent', 'Marc-Sald', 'Bio-Ent', 'Bio-Sal', 'Tardanza', 'SalioTempr', 'Observacion']
    if param_tipo == 'json':
        data_head.pop(0)

    data.append(data_head)

    cglobal = ConfiguracionGlobal.objects.all().first()
    xdia_name = {'6': 'domingo', '0': 'lunes', '1': 'martes', '2': 'miercoles', '3': 'jueves', '4': 'viernes', '5': 'sabado'}

    if param_fecha_ini and param_fecha_fin:
        fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y')
        fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y') + timedelta(days=1)

        # print param_uid.username
        usuarios = User.objects.filter(username=param_uid, habilitado_marcar=True)
        if not usuarios:
            usuarios = User.objects.filter(habilitado_marcar=True).exclude(username='admin')

        # for fecha in daterange(fecha_ini, fecha_fin):
            # for usuario in usuarios:#
        for usuario in usuarios:
            for fecha in daterange(fecha_ini, fecha_fin):
                # print fecha_item.strftime("%Y-%m-%d")
                # items = RegistroHorario.objects.all()
                dia_semana = str(WEEKDAYS_ABBR[fecha.weekday()]).decode('utf-8')
                # dia_semana = xdia_name[str(fecha.weekday())]
                # if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]):
                    # print 'no habil'

                # print usuario
                # print fecha
                items = RegistroHorario.objects.filter(fecha=fecha, usuario=usuario).order_by('horario_rango__hora_entrada').distinct('horario_rango__hora_entrada')
                # print items

                # for ditem in items:
                    # print ditem

                feriado = Feriado.objects.filter(fecha=fecha).first()

                # permisos = DesignacionPermiso.objects.filter(fecha_ini__lte=fecha, fecha_fin__gte=fecha, usuario__username=param_uid).exclude(estado='rechazado')
                permisos = DesignacionPermiso.objects.filter(fecha_ini__lte=fecha, fecha_fin__gte=fecha, usuario=usuario, estado__in=['aprobado_rrhh', 'aprobado_jefe'])
                # print "%s>>%s"%(fecha, permisos)
                # dia_semana
                # print xdia_name[str(fecha.weekday())]
                # AQUI ME QUEDE BUSCANDO ESTAS FECHAS
                # print getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())])

		# sw_dia_habil = False
                for datos_marcaciones in marcaciones_parse_horas(cglobal, xdia_name, fecha, items, feriado, dia_semana, permisos, param_tipo, usuario):
                    if datos_marcaciones[13]:
                        datos_marcaciones[13] = strfdelta(datos_marcaciones[13])

                    if datos_marcaciones[14]:
                        datos_marcaciones[14] = strfdelta(datos_marcaciones[14])

                    if param_tipo == 'json':
                        datos_marcaciones.pop(0)
                    data.append(datos_marcaciones)

                    # sw_dia_habil = True



                # if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]):
                #
                #     if items:
                #         for item in items:
                #             print "--------------------------------------------"
                #             print item.id
                #             print item.fecha
                #             print item.horario_rango
                #
                #             tardanza = ''
                #             if not item.hora_entrada == None and item.hora_entrada > item.horario_rango.hora_entrada:
                #                 tardanza = datetime.combine(date.today(), item.hora_entrada) - datetime.combine(date.today(), item.horario_rango.hora_entrada)
                #
                #             temprano = ''
                #             if not item.hora_salida == None and item.hora_salida < item.horario_rango.hora_salida:
                #                 temprano = datetime.combine(date.today(), item.horario_rango.hora_salida) - datetime.combine(date.today(), item.hora_salida)
                #
                #             # sw_tolerancia_acumulada = False
                #             # tolerancia = timedelta(0)
                #             # if items:
                #             #     tolerancia = timedelta(minutes=items.first().horario_rango.horario.tolerancia)
                #             #     sw_tolerancia_acumulada = items.first().horario_rango.horario.sw_tolerancia_acumulada
                #             # from operator import xor
                #             # tardanza_time = datetime.strptime(tardanza, '%H:%M:%S').time()
                #             # print strfdelta(tardanza, "{days} days {hours}:{minutes}:{seconds}")
                #             tardanza_str = ''
                #             if tardanza:
                #                 tardanza_str = strfdelta(tardanza)
                #
                #             temprano_str = ''
                #             if temprano:
                #                 temprano_str = strfdelta(temprano)
                #
                #             data_body = [
                #                 item.usuario.ci if item.usuario.ci else '',
                #                 item.usuario.username,
                #                 ('%s %s')%(item.usuario.first_name, item.usuario.last_name),
                #                 dia_semana,
                #                 item.fecha.strftime('%d/%m/%Y'),
                #                 # item.horario_rango.descripcion,
                #                 # Si es feriado mostrar el titulo del feriado y marcar con X  if-> (Verficar si es dia Habil de Marcacion)
                #                 (feriado.descripcion if feriado else item.horario_rango.descripcion) if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]) else 'sin horario',
                #                 'X' if feriado else '',
                #
                #                 render_time(item.horario_rango.hora_entrada),
                #                 render_time(item.horario_rango.hora_salida),
                #                 render_time(item.hora_entrada),
                #                 render_time(item.hora_salida),
                #                 item.biometrico_entrada,
                #                 item.biometrico_salida,
                #                 # datetime.strptime(registro['hora'], '%H:%M:%S').time()
                #
                #                 # '' if permisos else str(tardanza.strftime('%H:%M')),
                #                 '' if permisos else str(tardanza_str),
                #                 '' if permisos else str(temprano_str),
                #                 # 'Falta 2222' if item.biometrico_entrada or item.biometrico_salida else 'Falto'
                #                 'CON COMISION' if permisos else ('FALTA' if not (item.biometrico_entrada and item.biometrico_salida) else '')
                #             ]
                #             if param_tipo == 'json':
                #                 tolerancia = item.horario_rango.horario.tolerancia
                #                 sw_tolerancia_acumulada = item.horario_rango.horario.sw_tolerancia_acumulada
                #                 # TENGO QUE APLICAR EL CALCULO DE TOLERANCIA AQUI
                #
                #                 # sw_tolerancia_acumulada = item.horario_rango.horario.sw_tolerancia_acumulada
                #                 # if not sw_tolerancia_acumulada:
                #                 #     tolerancia = item.horario_rango.tolerancia_individual
                #                 #
                #                 data_body.pop(0)
                #                 data_body.append(tolerancia)
                #                 data_body.append(sw_tolerancia_acumulada)
                #
                #             data.append(data_body)
                #
                #     else:
                #         data_body = [
                #             usuario.ci if usuario.ci else '',
                #             usuario.username,
                #             ('%s %s')%(usuario.first_name, usuario.last_name),
                #             dia_semana,
                #             fecha.strftime('%d/%m/%Y'),
                #             # 'FALTO',
                #             # (Si es feriado mostrar el titulo del feriado y marcar con X) if-> (Verficar si es dia Habil de Marcacion)
                #             (feriado.descripcion if feriado else 'FALTO') if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]) else 'SIN HORARIO',
                #             'X' if feriado else '',
                #
                #             '',
                #             '',
                #             '',
                #             '',
                #             '',
                #             '',
                #             '',
                #             '',
                #             ''
                #         ]
                #         if param_tipo == 'json':
                #             data_body.pop(0)
                #             data_body.append(0)
                #             data_body.append(False)
                #
                #         data.append(data_body)
                    # print param_uid

                if permisos:
                    for permiso in permisos:
                        # print permiso
                        motivo_permiso = ''
                        if permiso.tipo_permiso == "olvido_marcar":
                            motivo_permiso = permiso.motivo

                        if permiso.tipo_permiso == "comision":
                            motivo_permiso = permiso.motivo

                        if permiso.tipo_permiso == "licencia":
                            motivo_permiso = permiso.motivo

                        data_body = [
                            permiso.usuario.ci if permiso.usuario.ci else '',
                            permiso.usuario.username,
                            ('%s %s')%(permiso.usuario.first_name, permiso.usuario.last_name),
                            dia_semana,
                            fecha.strftime('%d/%m/%Y'),
                            # item.horario_rango.descripcion,
                            # Si es feriado mostrar el titulo del feriado y marcar con X  if-> (Verficar si es dia Habil de Marcacion)
                            permiso.tipo_permiso.upper(),
                            motivo_permiso,
                            '',
                            '',
                            render_time(permiso.hora_ini),
                            '' if permiso.tipo_permiso == "olvido_marcar" else render_time(permiso.hora_fin),
                            '',
                            '',
                            '',
                            '',
                            # 'Falta 2222' if item.biometrico_entrada or item.biometrico_salida else 'Falto todo'
                            # 'Falta' if not (item.biometrico_entrada and item.biometrico_salida) else ''
                            'PERMISO',
                        ]
                        if param_tipo == 'json':
                            data_body.pop(0)
                            data_body.append(0)
                            data_body.append(False)

                        data.append(data_body)

    filename_time = 'reporte_asistencia_%s' % timezone.now().strftime('%d-%m-%Y')
    if param_tipo == 'json':
        return HttpResponse(json.dumps(data), content_type="application/json")
    elif param_tipo == 'csv':
        return excel.make_response_from_array(data, 'csv', file_name=filename_time)
    elif param_tipo == 'xls':
        return excel.make_response_from_array(data, 'xls', file_name=filename_time)
    else:
        return excel.make_response_from_array(data, 'ods', file_name=filename_time)


# ----------------------------------------------------------------------------
def reporte_marcaciones_consolidado2(request):
    param_tipo = request.GET.get('tipo')
    # if param_tipo == 'json':
    #     if individual:
    #         user_jwt = PermissionViewJWT(request).is_authenticated()
    #         param_uid = user_jwt
    #     else:
    #         PermissionViewJWT(request).permission_required_groups(['RRHH', ])
    #         param_uid = request.GET.get('uid')
    # else:
    #     param_uid = request.GET.get('uid')
    param_uid = request.GET.get('uid')
    param_fecha_ini = request.GET.get('fecha_ini')
    param_fecha_fin = request.GET.get('fecha_fin')

    data = []
    cglobal = ConfiguracionGlobal.objects.all().first()
    xdia_name = {'6': 'domingo', '0': 'lunes', '1': 'martes', '2': 'miercoles', '3': 'jueves', '4': 'viernes', '5': 'sabado'}

    if param_fecha_ini and param_fecha_fin:
        fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y')
        fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y') + timedelta(days=1)
        # DATA HEAD --------------------------------------------------------------------
        data_head = ['', '']
        data_head_fecha = ['', '']
        cglobal = ConfiguracionGlobal.objects.all().first()
        xdia_shortname = {6: 'dom', 0: 'lun', 1: 'mar', 2: 'mie', 3: 'jue', 4: 'vie', 5: 'sab'}
        xdia_name = {'6': 'domingo', '0': 'lunes', '1': 'martes', '2': 'miercoles', '3': 'jueves', '4': 'viernes', '5': 'sabado'}
        for fecha in daterange(fecha_ini, fecha_fin):
            if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]):
                data_head.append(xdia_shortname[fecha.weekday()])
                data_head_fecha.append(fecha.strftime('%d/%m/%Y'))

        data.append(data_head)
        data_head = ['UID', 'Nombre']
        for fecha in daterange(fecha_ini, fecha_fin):
            if getattr(cglobal, 'horario_%s' % xdia_name[str(fecha.weekday())]):
                data_head.append(fecha.day)

        data_head.append('Min Acum.')
        data.append(data_head)

        # DATA BODY --------------------------------------------------------------------
        usuarios = User.objects.filter(username=param_uid, habilitado_marcar=True)
        if not usuarios:
            usuarios = User.objects.filter(habilitado_marcar=True).exclude(username='admin')

        for usuario in usuarios:
            data_body = [usuario.username, '%s %s' % (usuario.first_name, usuario.last_name)]
            sum_tardanza = timedelta(0)
            sw_incidente_global = False
            for fecha in daterange(fecha_ini, fecha_fin):
                dia_semana = str(WEEKDAYS_ABBR[fecha.weekday()]).decode('utf-8')
                items = RegistroHorario.objects.filter(fecha=fecha, usuario=usuario)
                feriado = Feriado.objects.filter(fecha=fecha).first()
                # permisos = DesignacionPermiso.objects.filter(fecha_ini__lte=fecha, fecha_fin__gte=fecha, usuario__username=param_uid, estado='aprobado_rrhh')
                permisos = DesignacionPermiso.objects.filter(fecha_ini__lte=fecha, fecha_fin__gte=fecha, usuario=usuario, estado__in=['aprobado_rrhh', 'aprobado_jefe'])

                sw_observacion = None
                tardanza = timedelta(0)
                sw_dia_habil = False
		# print "%s -- %s >> %s"%(fecha, permisos, usuario)

                for datos_marcaciones in marcaciones_parse_horas(cglobal, xdia_name, fecha, items, feriado, dia_semana, permisos, param_tipo, usuario):
                    # print "%s << %s <<<"%(fecha, datos_marcaciones[15])
                    if datos_marcaciones[13] and not datos_marcaciones[14] and not datos_marcaciones[15]:
                        tardanza += datos_marcaciones[13]
                        # tardanza += datetime.strptime(datos_marcaciones[13], '%H:%M').time()
                    if datos_marcaciones[14]:
                        sw_observacion = 'T'
                        sw_incidente_global = True
                    if datos_marcaciones[15] == 'CON PERMISO':
                        sw_observacion = 'P'
                    if datos_marcaciones[15] == 'FALTA':
                        sw_observacion = 'F'
                        sw_incidente_global = True
                    if param_tipo == 'json':
                        datos_marcaciones.pop(0)

                    sw_dia_habil = True
                    # print "%s << %s <<<"%(fecha, datos_marcaciones[14])

                if sw_dia_habil:
                    if not sw_observacion:
                        tardanza_str = strfdelta(tardanza)
                        # print "<*****************************-"
                        data_body.append(str(tardanza_str))
                        sum_tardanza += tardanza
                    else:
                        # print "%s<-------------"%sw_observacion
                        data_body.append(str(sw_observacion))
                        # sum_tardanza += 0

            print data_body

            if sw_incidente_global:
                data_body.append(str(''))
                data_body.append(str('El usuario tiene faltas y/o salio temprano'))
            else:
                sum_tardanza_str = strfdelta(sum_tardanza)
                data_body.append(str(sum_tardanza_str))
                data_body.append(str(''))

            data.append(data_body)

    filename_time = 'reporte_asistencia_consolidado_%s' % timezone.now().strftime('%d-%m-%Y')
    if param_tipo == 'json':
        return HttpResponse(json.dumps(data), content_type="application/json")
    elif param_tipo == 'csv':
        return excel.make_response_from_array(data, 'csv', file_name=filename_time)
    elif param_tipo == 'xls':
        return excel.make_response_from_array(data, 'xls', file_name=filename_time)
    else:
        return excel.make_response_from_array(data, 'ods', file_name=filename_time)
# ----------------------------------------------------------------------------


def reporte_marcaciones_consolidado(request):
    param_fecha_ini = request.GET.get('fecha_ini')
    param_fecha_fin = request.GET.get('fecha_fin')
    param_uid = request.GET.get('uid')
    param_tipo = request.GET.get('tipo')

    if param_fecha_ini and param_fecha_fin:
        fecha_ini = datetime.strptime(param_fecha_ini, '%d/%m/%Y')
        fecha_fin = datetime.strptime(param_fecha_fin, '%d/%m/%Y') + timedelta(days=1)

    data = []

    data_head = ['', '']
    data_head_fecha = ['', '']
    cglobal = ConfiguracionGlobal.objects.all().first()
    xdia_name = {6: 'dom', 0: 'lun', 1: 'mar', 2: 'mie', 3: 'jue', 4: 'vie', 5: 'sab'}
    xdia_fullname = {'6': 'domingo', '0': 'lunes', '1': 'martes', '2': 'miercoles', '3': 'jueves', '4': 'viernes', '5': 'sabado'}
    for fecha in daterange(fecha_ini, fecha_fin):
        if getattr(cglobal, 'horario_%s' % xdia_fullname[str(fecha.weekday())]):
            data_head.append(xdia_name[fecha.weekday()])
            data_head_fecha.append(fecha.strftime('%d/%m/%Y'))

    data.append(data_head)
    data_head = ['UID', 'Nombre']
    for fecha in daterange(fecha_ini, fecha_fin):
        if getattr(cglobal, 'horario_%s' % xdia_fullname[str(fecha.weekday())]):
            data_head.append(fecha.day)

    data_head.append('Min Acum.')

    data.append(data_head)
    usuarios = User.objects.filter(username=param_uid, habilitado_marcar=True)
    if not usuarios:
        usuarios = User.objects.filter(habilitado_marcar=True).exclude(username='admin')

    for usuario in usuarios:
        data_body = [usuario.username, '%s %s' % (usuario.first_name, usuario.last_name)]
        sum_tardanza = timedelta(0)

        sw_incidente_global = False
        for fecha in daterange(fecha_ini, fecha_fin):
            if getattr(cglobal, 'horario_%s' % xdia_fullname[str(fecha.weekday())]):
                items = RegistroHorario.objects.filter(fecha=fecha, usuario=usuario)
                tardanza = timedelta(0)

                sw_tolerancia_acumulada = False
                tolerancia = timedelta(0)

                sw_incidente = None
                if items:
                    tolerancia = timedelta(minutes=items.first().horario_rango.horario.tolerancia)
                    sw_tolerancia_acumulada = items.first().horario_rango.horario.sw_tolerancia_acumulada
                else:
                    sw_incidente = 'F'
                    sw_incidente_global = True


                # print fecha
                # print len(items)


                for item in items:
                    if not item.hora_entrada == None and item.hora_entrada > item.horario_rango.hora_entrada:
                        tardanza += datetime.combine(date.today(), item.hora_entrada) - datetime.combine(date.today(), item.horario_rango.hora_entrada)

                    if not item.hora_salida == None and item.hora_salida < item.horario_rango.hora_salida:
                        sw_incidente = 'T'
                        sw_incidente_global = True

                    if not sw_tolerancia_acumulada:
                        # tolerancia_individual = timedelta(minutes=items.first().horario_rango.tolerancia_individual)
                        # if tardanza > tolerancia_individual:
                        #     tardanza -= tolerancia_individual
                        # else:
                        #     tardanza = timedelta(0)
                        if tardanza > tolerancia:
                            tardanza -= tolerancia
                        else:
                            tardanza = timedelta(0)

                if sw_tolerancia_acumulada:
                    # tolerancia = timedelta(minutes=items.first().horario_rango.horario.tolerancia)
                    if tardanza > tolerancia:
                        tardanza -= tolerancia
                    else:
                        tardanza = timedelta(0)

                tardanza_str = ''
                if tardanza:
                    tardanza_str = strfdelta(tardanza)
                # data_body.append(str(tardanza))

                if not sw_incidente:
                    data_body.append(str(tardanza_str))
                    sum_tardanza += tardanza
                else:
                    data_body.append(str(sw_incidente))
                    # sum_tardanza += 0

        sum_tardanza_str = ''
        if sum_tardanza:
            sum_tardanza_str = strfdelta(sum_tardanza)
        data_body.append(str(sum_tardanza_str))

        if sw_incidente_global:
            # me quede haca
            data_body.append(str('El usuario tiene faltas y/o salio temprano'))

        data.append(data_body)

    filename_time = 'reporte_asistencia_%s' % timezone.now().strftime('%d-%m-%Y')

    # PRE HEAD
    if param_tipo == 'json':
        data.insert(0, data_head_fecha)
    else:
        data.insert(0, ["PLANILLA DE ASISTENCIA"])
        data.insert(1, ["DEL %s AL %s" % (fecha_ini.strftime('%d/%m/%Y'), fecha_fin.strftime('%d/%m/%Y'))])
        data.insert(2, [""])

    if param_tipo == 'json':
        return HttpResponse(json.dumps(data), content_type="application/json")
    elif param_tipo == 'csv':
        return excel.make_response_from_array(data, 'csv', file_name=filename_time)
    elif param_tipo == 'xls':
        return excel.make_response_from_array(data, 'xls', file_name=filename_time)
    else:
        return excel.make_response_from_array(data, 'ods', file_name=filename_time)

# ----------------------------------------------------------------------------------------------
# REPORT PDF
# ----------------------------------------------------------------------------------------------
from xhtml2pdf import pisa
import cStringIO as StringIO
import cgi
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404


def get_full_path_x(request):
    full_path = ('http', ('', 's')[request.is_secure()], '://', request.META['HTTP_HOST'], request.path)
    return ''.join(full_path)


def generar_pdf(request, html):

    import ssl
    ssl._create_default_https_context = ssl._create_unverified_context

    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8', path=get_full_path_x(request))
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('Error al generar el PDF: %s' % cgi.escape(html))


def report_permiso(request, id):
    PermissionViewJWT(request).permission_required_groups(['RRHH', 'JEFE DE UNIDAD', 'FUNCIONARIO',])

    report_template = {
        'comision': 'control_personal/reportes/comision.html',
        'particular': 'control_personal/reportes/particular.html',
        'licencia': 'control_personal/reportes/licencia.html',
        'olvido_marcar': 'control_personal/reportes/olvido_marcar.html',
    }
    item = get_object_or_404(DesignacionPermiso, id=int(id))
    dias = item.fecha_fin - item.fecha_ini
    item.num_dias = dias.days+1
    html = render_to_string(report_template[item.tipo_permiso], {'pagesize': 'A4', 'item': item}, context_instance=RequestContext(request))
    return generar_pdf(request, html)


from django.shortcuts import render
import calendar
from calendar import HTMLCalendar
from itertools import groupby


def calendarioDias(items):
    dias = []
    for item in items:
        if item.tipo_vacacion in ['completo','medio']:
            # print "%s <--> %s"%(item.fecha_ini, item.fecha_fin)
            for dia in daterange(item.fecha_ini, item.fecha_fin + timedelta(days=1)):
                dias.append(dia.day)
        if item.tipo_vacacion == 'horas':
            dias.append(item.fecha_ini.day)
    return dias


# class VacacionCalendario(HTMLCalendar):
class VacacionCalendario(calendar.LocaleHTMLCalendar):
    def __init__(self, marcados):
        super(VacacionCalendario, self).__init__()
        self.marcados = marcados

    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            dayItem = date(self.year, self.month, day)
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today'
            xmarcado = self.marcados.filter(fecha_ini__lte=dayItem, fecha_fin__gte=dayItem).first()
            # print xmarcado
            if xmarcado:
                if xmarcado.tipo_vacacion == "completo":
                    cssclass += ' completo'
                if xmarcado.tipo_vacacion == "medio":
                    cssclass += ' medio'
            return self.day_cell(cssclass, day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(VacacionCalendario, self).formatmonth(year, month)

    # def group_by_day(self, marcados):
    #     field = lambda workout: workout.performed_at.day
    #     return dict(
    #         [(day, list(items)) for day, items in groupby(marcados, field)]
    #     )

    def day_cell(self, cssclass, body):
        return '<td class="%s">%s</td>' % (cssclass, body)


from dateutil.relativedelta import relativedelta


def report_vacacion(request):
    # PermissionViewJWT(request).permission_required_groups(['RRHH', 'JEFE DE UNIDAD', 'FUNCIONARIO',])
    user_jwt = PermissionViewJWT(request).is_authenticated()
    cglobal = ConfiguracionGlobal.objects.all().first()

    # print cglobal.form_enable_vacaciones
    # print timezone.now().month

    # XMESES = {1: 'enero', 2: 'febrero', 3: 'marzo', 4: 'abril', 5: 'mayo', 6: 'junio', 7: 'julio', 8: 'agosto', 9: 'septiembre', 10: 'octubre', 11: 'noviembre', 12:'diciembre'}

    # # print XMESES[timezone.now().month]
    # if XMESES[timezone.now().month] in cglobal.form_enable_vacaciones:
	# year_calendar = timezone.now().year+1
    # else:
	# year_calendar = timezone.now().year

    # print year_calendar

    year_calendar = ConfiguracionGlobal.objects.yearActivo()
    xusuario = User.objects.filter(username=user_jwt, habilitado_marcar=True).first()
    vacacion = DesignacionVacacion.objects.filter(usuario=xusuario, fecha_ini__year=year_calendar, fecha_fin__year=year_calendar, estado__in=["aprobado",])

    xusuario.fecha_programacion = xusuario.fecha_asignacion + relativedelta(years=1, days=1)
    xusuario.dias_total_cas = User.vacacion.casTotal(xusuario.id)
    xcalendario = []
    for mes_calendar in range(1, 13):
       xcalendario.append(VacacionCalendario(vacacion).formatmonth(year_calendar, mes_calendar))

    vacacion.calendario = xcalendario
    html = render_to_string('control_personal/reportes/vacacion.html', {'pagesize': 'A4', 'item': xusuario, 'calendario': xcalendario, 'xfecha':'30/11/2016'}, context_instance=RequestContext(request))
    return generar_pdf(request, html)


def report_fichapersonal(request):
    user_jwt = PermissionViewJWT(request).is_authenticated()

    xusuario = User.objects.filter(username=user_jwt, habilitado_marcar=True).first()
    xitem = FichaPersonalHistorico.objects.filter(usuario__username=user_jwt).first()
    # xusuario = User.objects.filter(username='usuario', habilitado_marcar=True).first()
    # xitem = FichaPersonalHistorico.objects.filter(usuario__username='usuario').first()
    zitem = {}
    if xitem:
        zitem = json.loads(xitem.ficha_personal)
        # print zitem
    # return render(request, 'control_personal/reportes/ficha_personal.html', {'pagesize': 'A4', 'item': zitem, 'uitem':xusuario},context_instance=RequestContext(request))
    html = render_to_string('control_personal/reportes/ficha_personal.html', {'pagesize': 'A4', 'item': zitem, 'uitem':xusuario},context_instance=RequestContext(request))
    return generar_pdf(request, html)
