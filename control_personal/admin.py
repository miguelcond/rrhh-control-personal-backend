from django.contrib import admin
from models import Funcionario, Horario, HorarioRango, DesignacionHorario, RegistroHorario, RegistroHorarioRevision, Feriado, UnidadOrganizacional, DesignacionPermiso, ConfiguracionGlobal, DesignacionVacacion, FichaPersonal, FichaTrayectoria, FichaEstudiosUniversitarios, FichaEstudiosEspecializacion, FichaCursosRelacionados, FichaIdiomas, FichaConocimientos, FichaExperienciaLaboral, FichaPersonalHistorico
# from models import User
from models import Sugerencia


# Register your models here.
# admin.site.register(User)
admin.site.register(Funcionario)
admin.site.register(Horario)
admin.site.register(HorarioRango)
admin.site.register(DesignacionHorario)
admin.site.register(RegistroHorario)
admin.site.register(RegistroHorarioRevision)
admin.site.register(Feriado)
admin.site.register(UnidadOrganizacional)
admin.site.register(DesignacionPermiso)
admin.site.register(ConfiguracionGlobal)

# admin.site.register(Sugerencia)
class CustomDesignacionVacacion(admin.ModelAdmin):
    list_display = ('usuario', 'fecha_ini', 'fecha_fin', 'unidad_organizacional', 'fecha_registro')
    list_filter = ('unidad_organizacional', 'fecha_registro')
    search_fields = ['usuario__username']

admin.site.register(DesignacionVacacion, CustomDesignacionVacacion)

admin.site.register(FichaPersonal)
admin.site.register(FichaTrayectoria)
admin.site.register(FichaEstudiosUniversitarios)
admin.site.register(FichaEstudiosEspecializacion)
admin.site.register(FichaCursosRelacionados)
admin.site.register(FichaIdiomas)
admin.site.register(FichaConocimientos)
admin.site.register(FichaExperienciaLaboral)


admin.site.register(FichaPersonalHistorico)

# from django.contrib.auth.models import User
# from django.contrib.auth.admin import UserAdmin
#
#
# class CustomUserAdmin(UserAdmin):
#     UserAdmin.list_display += ('cargo',)
#     # UserAdmin.list_filter += ('cargo',)
#     UserAdmin.fieldsets += ('cargo',)
#
# admin.site.unregister(User)
# admin.site.register(User, UserAdmin)
