#!/usr/bin/python
# -*- coding: utf8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group
from django.utils.timezone import now
from datetime import datetime

# from jsonfield import JSONField
# from django.contrib.postgres.fields import JSONField


class UserManager(models.Manager):
    def casTotal(self, user_auth):
        xyears = 15
        item = self.filter(pk=user_auth, habilitado_marcar=True).first()
        if item.cas in range(0,5):
            xyears = 15
        if item.cas in range(5,10):
            xyears = 20
        if item.cas >= 10:
            xyears = 30
        return xyears


Group.add_to_class('menu', models.TextField(default='', blank=True, null=True))
User.add_to_class('cargo', models.CharField(max_length=250, blank=True, null=True))
User.add_to_class('ci', models.CharField(max_length=20, blank=True, null=True))
User.add_to_class('habilitado_marcar', models.BooleanField(default=False, verbose_name='Habilitado a Marcar'))

User.add_to_class('fecha_asignacion', models.DateField(blank=True, null=True))
User.add_to_class('cas', models.IntegerField(blank=True, null=True))
User.add_to_class('nro_item', models.IntegerField(blank=True, null=True))
User.add_to_class('unidad_dependencia', models.CharField(max_length=250, blank=True, null=True))

User.add_to_class('vacacion', UserManager())


# TRAYECTORIA LABORAL
YEAR_CHOICES = []
pnow = datetime.now()
for y in range(1960, pnow.year+1):
    YEAR_CHOICES.append((y, y))


TIPO_TRAYECTORIA = (
    ('seleccion', 'Proceso de Selección'),
    ('promocion', 'Promoción'),
    ('rotacion', 'Rotación'),
    ('rotacion', 'Transferencia')
)

class FichaTrayectoria(models.Model):
    fecha_inicio = models.DateField(verbose_name='Fecha de inicio')
    fecha_final = models.DateField(verbose_name='Fecha final')
    cargo = models.CharField(max_length=250, verbose_name='Cargo')
    # tipo_trayectoria = models.CharField(max_length=12, choices=TIPO_TRAYECTORIA, verbose_name='Tipo')

# ESTUDIOS UNIVERSITARIOS
class FichaEstudiosUniversitarios(models.Model):
    year_inicio = models.IntegerField(choices=YEAR_CHOICES, verbose_name='Año de inicio')
    year_final = models.IntegerField(choices=YEAR_CHOICES, verbose_name='Año final')
    carrera = models.CharField(max_length=200, verbose_name='Carrera o Área de Formación')
    universidad = models.CharField(max_length=200, verbose_name='Universidad o Instituto')
    concluido = models.BooleanField(verbose_name='Concluido')
    nivel = models.CharField(max_length=200, verbose_name='Nivel alcanzado')
    ciudad = models.CharField(max_length=70, verbose_name='Ciudad o País')
    titulo = models.BooleanField(verbose_name='Título Académico')
    titulo_provision = models.BooleanField(verbose_name='Título en Provisión Nacional')
    fecha_emision = models.DateField(verbose_name='Fecha emisión')


# ESTUDIOS DE ESPECIALIZACION
class FichaEstudiosEspecializacion(models.Model):
    year_inicio = models.IntegerField(choices=YEAR_CHOICES, verbose_name='Año de inicio')
    year_final = models.IntegerField(choices=YEAR_CHOICES, verbose_name='Año final')
    nivel = models.CharField(max_length=70, verbose_name='Nivel', help_text="Diplomado, Especializacion, Maestria, Doctorado")
    area = models.CharField(max_length=70, verbose_name='Área de especialización')
    carga_horaria = models.CharField(max_length=70, verbose_name='Carga horaria')
    universidad = models.CharField(max_length=70, verbose_name='Universidad o Instituto')
    concluido = models.BooleanField(verbose_name='Concluido')

# CURSOS RELACIONADOS
class FichaCursosRelacionados(models.Model):
    mes = models.IntegerField(verbose_name='Mes')
    year = models.IntegerField(choices=YEAR_CHOICES, verbose_name='Año')
    curso = models.CharField(max_length=70, verbose_name='Curso')
    area = models.CharField(max_length=70, verbose_name='Área')
    carga_horaria = models.CharField(max_length=70, verbose_name='Carga Horaria')
    universidad = models.CharField(max_length=70, verbose_name='Universidad o Instituto')
    concluido = models.BooleanField(verbose_name='Concluido')
    titulo = models.BooleanField(verbose_name='Título Académico')

# IDIOMAS
TIPO_NIVEL = (
    ('avanzado', 'Avanzado'),
    ('intermedio', 'Intermedio'),
    ('basico', 'Basico')
)
class FichaIdiomas(models.Model):
    descripcion = models.CharField(max_length=70, verbose_name='Descripción')
    nivel = models.CharField(max_length=15, choices=TIPO_NIVEL, verbose_name='Nivel')
    # lee = models.BooleanField(verbose_name='Lee')
    # escribe = models.BooleanField(verbose_name='Escribe')
    # habla = models.BooleanField(verbose_name='Habla')

# PAQUETES COMPUTACION
TIPO_GRADO = (
    ('alto', 'Alto'),
    ('medio', 'Medio'),
    ('bajo', 'Bajo')
)

class FichaConocimientos(models.Model):
    descripcion = models.CharField(max_length=70, verbose_name='Descripción')
    grado = models.CharField(max_length=10, choices=TIPO_GRADO, verbose_name='Grado  de Conocimiento')

# EXPERIENCIA LABORAL
class FichaExperienciaLaboral(models.Model):
    nombre = models.CharField(max_length=150, verbose_name='Nombre Institución')
    tipo = models.CharField(max_length=100, verbose_name='Tipo')
    pais = models.CharField(max_length=70, verbose_name='País')
    cargo = models.CharField(max_length=70, verbose_name='Cargo')
    fecha_inicio = models.DateField(verbose_name='Fecha de inicio')
    fecha_final = models.DateField(verbose_name='Fecha final')


TIPO_DOCUMENTO = (
    ('ci', 'CI'),
    ('pasaporte', 'Pasaporte'),
    ('otro', 'Otro')
)

class FichaPersonal(models.Model):
    usuario = models.ForeignKey(User)

    # DATOS PERSONALES
    telefono_domicilio = models.CharField(max_length=50, verbose_name='N° Telefónico Domicilio')
    telefono_celular = models.CharField(max_length=50, verbose_name='N° Telefónico Celular')
    fecha_nacimiento = models.DateField(verbose_name='Fecha de Nacimiento')
    email_personal = models.EmailField(verbose_name='Email Personal')
    documento_tipo = models.CharField(max_length=12, choices=TIPO_DOCUMENTO, verbose_name='Documento Tipo')
    documento_numero = models.CharField(max_length=50, verbose_name='Documento Número')
    documento_exp = models.CharField(max_length=20, verbose_name='Expedicion')
    pais_nacimiento = models.CharField(max_length=70, verbose_name='País Nacimiento')
    lugar_nacimiento = models.CharField(max_length=70, verbose_name='Lugar Nacimiento')
    nacionalidad = models.CharField(max_length=70, verbose_name='Nacionalidad')
    zona_domicilio = models.TextField(verbose_name='Zona domicilio')
    calle_domicilio = models.TextField(verbose_name='Av. o Calle Domicilio')
    nro_domicilio = models.CharField(max_length=20, verbose_name='N° Domicilio')
    edificio = models.CharField(max_length=70, verbose_name='Edificio')
    piso = models.CharField(max_length=20, verbose_name='Piso')
    nr_departamento = models.CharField(max_length=20, verbose_name='N° Departamento')
    nro_cuenta = models.CharField(max_length=70, verbose_name='N° Cuenta Bancaria')
    estado_civil = models.CharField(max_length=50, verbose_name='Estado Civil')
    grupo_sanguineo = models.CharField(max_length=10, verbose_name='Grupo Sanguineo')


    # DATOS LABORALES
    profesion = models.CharField(max_length=100, verbose_name='Profesión')
    cas = models.DateTimeField(verbose_name='CAS')
    registro_profesional = models.IntegerField(verbose_name='Registro profesional N°')
    item = models.IntegerField(verbose_name='Item')
    email_institucional = models.EmailField(verbose_name='Email Institucional')
    cargo_actual = models.CharField(max_length=200, verbose_name='Cargo Actual')
    unidad_actual = models.CharField(max_length=200, verbose_name='Unidad Organizacional')
    nro_funcionario_carrera = models.IntegerField(verbose_name='N° Funcionario de Carrera')

    # TRAYECTORIA LABORAL
    ficha_trayectoria = models.ManyToManyField(FichaTrayectoria)
    # ESTUDIOS UNIVERSITARIOS
    ficha_estudios_universitarios = models.ManyToManyField(FichaEstudiosUniversitarios)
    # ESTUDIOS DE ESPECIALIZACION
    ficha_estudios_especializacion = models.ManyToManyField(FichaEstudiosEspecializacion)
    # CURSOS RELACIONADOS
    ficha_cursos_relacionados = models.ManyToManyField(FichaCursosRelacionados)
    # IDIOMAS
    ficha_idiomas = models.ManyToManyField(FichaIdiomas)
    # PAQUETES COMPUTACION
    ficha_conocimientos = models.ManyToManyField(FichaConocimientos)
    # EXPERIENCIA LABORAL
    ficha_experiencia_laboral = models.ManyToManyField(FichaExperienciaLaboral)

    # FECHA DE REGISTRO
    fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', default=now)


from jsonfield import JSONField


class FichaPersonalHistorico(models.Model):
    usuario = models.ForeignKey(User)
    ficha_personal = JSONField()
    # ficha_personal = models.TextField(verbose_name='Ficha Personal')
    fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', default=now)

    class Meta:
        permissions = (
            ("view_fichapersonalhistorico", "Puede ver los registros"),
            ("options_fichapersonalhistorico", "Puede ver los options"),
        )

    def __str__(self):
        return self.usuario

    def __unicode__(self):
        return unicode(self.usuario)



# def user_unicode_patch(self):
#     return '%s %s' % (self.first_name, self.last_name)
#
# User.__unicode__ = user_unicode_patch
# User.__str__ = user_unicode_patch



# from django.contrib.auth.models import AbstractBaseUser
# class User(AbstractBaseUser):
#     cargo = models.CharField(max_length=250)
#
#     class Meta:
#         permissions = (
#             ("view_horario", "Puede ver los registros"),
#             ("options_horario", "Puede ver los options"),
#         )
# Group.add_to_class('menu', JSONField())

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
# from django.db.models.signals import pre_migrate
from django.contrib.auth import models as auth_models
from django.dispatch import receiver


# @receiver(pre_migrate, sender=auth_models)
# def add_user_permissions(sender, **kwargs):
#     content_type = ContentType.objects.get_for_model(settings.AUTH_USER_MODEL)
#     Permission.objects.get_or_create(codename='view_user', name='Puede ver los registros', content_type=content_type)
    # Permission.objects.get_or_create(codename='options_user', name='Puede ver los options', content_type=content_type)

class Funcionario(models.Model):
    # user = models.OneToOneField(User)
    uid = models.CharField(max_length=50)
    estado = models.BooleanField(default=False, help_text='El usuario esta activo en el sistema')


# ASIGNACION DE HORARIO QUE EL FUNCIONARIO DEBE CUMPLIR
class Horario(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción')
    observacion = models.TextField(verbose_name='Observación')
    # estado = models.BooleanField(default=False, help_text='Un libro es considerado de Referencia si')
    sw_tolerancia_acumulada = models.BooleanField(default=True, verbose_name='Activar tolerancia acumulada', help_text='Trabajar con el modo de tolerancia acumulada')
    # tolerancia_acumulada = models.IntegerField(default=0, verbose_name='Tolerancia acumulada en minutos')
    tolerancia = models.IntegerField(default=0, verbose_name='Tolerancia en minutos')

    class Meta:
        permissions = (
            ("view_horario", "Puede ver los registros"),
            ("options_horario", "Puede ver los options"),
        )

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion


class HorarioRango(models.Model):
    horario = models.ForeignKey(Horario)
    descripcion = models.CharField(max_length=100, verbose_name='Descripción')
    hora_entrada = models.TimeField(verbose_name='Hora de entrada (HH:MM)')
    hora_salida = models.TimeField(verbose_name='Hora de salida (HH:MM)')
    rango_entrada_ini = models.TimeField(verbose_name='Rango de entrada mínima (HH:MM:SS)')
    rango_entrada_fin = models.TimeField(verbose_name='Rango de entrada máxima (HH:MM:SS)')
    rango_salida_ini = models.TimeField(verbose_name='Rango de salida mínima (HH:MM:SS)')
    rango_salida_fin = models.TimeField(verbose_name='Rango de salida máxima (HH:MM:SS)')
    # tolerancia_individual = models.IntegerField(default=0, verbose_name='Tolerancia individual en minutos', help_text='Si el modo de tolerancia acumulada se encuentra desactivada puede utilizar este tipo de tolerancia, caso contrario dejar en 0')

    class Meta:
        permissions = (
            ("view_horariorango", "Puede ver los registros"),
            ("options_horariorango", "Puede ver los options"),
        )

    def clean(self):
        if self.hora_entrada <= self.rango_entrada_ini:
            raise ValidationError('Error la hora de entrada debe ser mayor al rango de entrada de inicio')

        if self.hora_entrada > self.hora_salida:
            raise ValidationError('Error la hora de entrada debe ser menor o igual a la hora de salida')

        if self.hora_salida >= self.rango_salida_fin:
            raise ValidationError('Error la hora de salida debe ser menor al rango de salida final')

        if self.rango_entrada_ini > self.rango_entrada_fin:
            raise ValidationError('Error el rango de entrada de inicio debe ser menor o igual al rango de entrada final')

        if self.rango_entrada_fin >= self.rango_salida_ini:
            raise ValidationError('Error el rango de entrada debe ser menor al rango de salida')

        if self.rango_salida_ini > self.rango_salida_fin:
            raise ValidationError('Error el rango de salida de inicio debe ser menor o igual al rango de salida final')


    def __str__(self):
        return "%s: %s - %s" % (self.horario, self.hora_entrada, self.hora_salida)

    def __unicode__(self):
        return "%s: %s - %s" % (self.horario, self.hora_entrada, self.hora_salida)


class DesignacionHorario(models.Model):
    # usuario = models.ForeignKey(User)
    usuario = models.ManyToManyField(User)
    horario = models.ForeignKey(Horario)
    fecha_ini = models.DateField(verbose_name='Fecha de inicio')
    fecha_fin = models.DateField(verbose_name='Fecha final')
    designar_lunes = models.BooleanField(default=True, verbose_name='Designar en Lunes', help_text='Marque la casilla para efectuar la designación')
    designar_martes = models.BooleanField(default=True, verbose_name='Designar en Martes', help_text='Marque la casilla para efectuar la designación')
    designar_miercoles = models.BooleanField(default=True, verbose_name='Designar en Miercoles', help_text='Marque la casilla para efectuar la designación')
    designar_jueves = models.BooleanField(default=True, verbose_name='Designar en Jueves', help_text='Marque la casilla para efectuar la designación')
    designar_viernes = models.BooleanField(default=True, verbose_name='Designar en Viernes', help_text='Marque la casilla para efectuar la designación')
    designar_sabado = models.BooleanField(default=False, verbose_name='Designar en Sabado', help_text='Marque la casilla para efectuar la designación')
    designar_domingo = models.BooleanField(default=False, verbose_name='Designar en Domingo', help_text='Marque la casilla para efectuar la designación')

    class Meta:
        permissions = (
            ("view_designacionhorario", "Puede ver los registros"),
            ("options_designacionhorario", "Puede ver los options"),
        )

    def clean(self):
        from django.core.exceptions import ValidationError
        if self.fecha_ini > self.fecha_fin:
            raise ValidationError('Error la fecha de inicio debe ser menor o igual a la fecha final')
# PROBAR SI FUNCIONA EN EL POSTMAN Y SI ME VOTA EL ERROR CON REST JSON

# class DesignacionHorarioUsuario(models.Model):
#     usuario = models.ForeignKey(User)
#     designacion_horario = models.ForeignKey(DesignacionHorario)


# REGISTRO DE ASISTENCIA DE USUARIO AL DIA
class RegistroHorario(models.Model):
    hora_entrada = models.TimeField(blank=True, null=True, verbose_name='Hora de entrada')
    hora_salida = models.TimeField(blank=True, null=True, verbose_name='Hora de salida')
    fecha = models.DateField()
    usuario = models.ForeignKey(User)
    biometrico_entrada = models.CharField(max_length=100, blank=True, null=True)
    biometrico_salida = models.CharField(max_length=100, blank=True, null=True)
    horario_rango = models.ForeignKey(HorarioRango)
    observacion = models.TextField(blank=True, null=True, verbose_name='Observación')
    # api_id_entrada = models.IntegerField()
    # api_id_salida = models.IntegerField()
    class Meta:
        permissions = (
            ("view_registrohorario", "Puede ver los registros"),
            ("options_registrohorario", "Puede ver los options"),
        )

    def __str__(self):
        return "(%s) %s: %s - %s" % (self.usuario.username, self.fecha, self.hora_entrada, self.hora_salida)

    def __unicode__(self):
        return "(%s) %s: %s - %s" % (self.usuario.username, self.fecha, self.hora_entrada, self.hora_salida)


# ULTIMA REVISION DE LA TABLA XYYY QUE CONTIENE LOS REGISTROS TOTALES DE TODOS LOS BIOMETRICOS
class RegistroHorarioRevision(models.Model):
    fecha = models.DateField()

    class Meta:
        permissions = (
            ("view_registrohorariorevision", "Puede ver los registros"),
            ("options_registrohorariorevision", "Puede ver los options"),
        )

    def __str__(self):
        return self.fecha

    def __unicode__(self):
        return self.fecha


# TABLA DE FERIADOS REGISTRADOS EN EL SISTEMA
class Feriado(models.Model):
    descripcion = models.CharField(max_length=200, verbose_name='Descripción')
    fecha = models.DateField()

    class Meta:
        permissions = (
            ("view_feriado", "Puede ver los registros"),
            ("options_feriado", "Puede ver los options"),
        )

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion


# REGISTRO DE PERMISOS
# class TipoPermiso(models.Model):
#     descripcion = models.CharField(max_length=200)
#
#     class Meta:
#         permissions = (
#             ("view_tipopermiso", "Puede ver los registros"),
#             ("options_tipopermiso", "Puede ver los options"),
#         )
#
#     def __str__(self):
#         return self.descripcion
#
#     def __unicode__(self):
#         return self.descripcion


class UnidadOrganizacional(models.Model):
    unidad_organizacional = models.CharField(max_length=250, verbose_name='Unidad organizacional')

    class Meta:
        permissions = (
            ("view_unidadorganizacional", "Puede ver los registros"),
            ("options_unidadorganizacional", "Puede ver los options"),
        )

    def __str__(self):
        return self.unidad_organizacional

    def __unicode__(self):
        return self.unidad_organizacional


BOOL_APROBADO = (
    (True, 'Aprobado'),
    (False, 'No Aprobado')
)

TIPO_PERMISO = (
    ('comision', 'Comision'),
    ('particular', 'Particular'),
    ('licencia', 'Licencia')
    # ('olvido_marcar', 'Olvido de marcado')
)

TIPO_PERMISO_ESTADO = (
    ('creado', 'Creado'),
    ('aprobado_jefe', 'Aprobado Jefe'),
    ('aprobado_rrhh', 'Aprobado RRHH'),
    ('rechazado', 'Rechazado')
)


class PermisoActivosPorUsuario(models.Manager):
    def for_user(self, user):
        return self.filter(usuario=user, aprobado=False)
    # def get_query_set(self):
    #     # print self.request.user
    #     print self.request.user
    #     return super(PermisoActivosPorUsuario, self).get_query_set().all()

    # def for_user(self, user):
    #     # if user.is_superuser:
    #     #     return super(PermisoActivosPorUsuario, self).get_query_set().all()
    #
    #     return super(PermisoActivosPorUsuario, self).get_query_set().filter(usuario=user, aprobado=False)


from django.utils import timezone
# from django.utils.timezone import now


class DesignacionPermiso(models.Model):
    # tipo_permiso = models.ForeignKey(TipoPermiso)
    tipo_permiso = models.CharField(max_length=15, choices=TIPO_PERMISO, verbose_name='Tipo de permiso')
    unidad_organizacional = models.ForeignKey(UnidadOrganizacional, verbose_name='Unidad organizacional')

    motivo = models.TextField(blank=True, null=True)

    comision_lugar = models.CharField(max_length=250, blank=True, null=True, verbose_name='Lugar')
    licencia_respaldo = models.CharField(max_length=250, blank=True, null=True, verbose_name='Documento de Respaldo')

    particular_numeral_iii = models.CharField(max_length=250, blank=True, null=True, verbose_name='Permiso Art. 31, Numeral III')
    particular_numeral_iv = models.CharField(max_length=250, blank=True, null=True, verbose_name='Permiso Art. 31, Numeral IV')

    usuario = models.ForeignKey(User)
    fecha_ini = models.DateField(verbose_name='Fecha Inicial (DD/MM/YYYY)')
    fecha_fin = models.DateField(verbose_name='Fecha Final (DD/MM/YYYY)')
    hora_ini = models.TimeField(blank=True, null=True, verbose_name='Hora de salida (HH:MM)')
    hora_fin = models.TimeField(blank=True, null=True, verbose_name='Hora de retorno (HH:MM)')
    jefe = models.ForeignKey(User, related_name='jefe')
    # token_mail = models.CharField(max_length=250, default='token')
    # token_confirmation_date = models.DateTimeField(blank=True, null=True)
    # aprobado = models.BooleanField(choices=BOOL_APROBADO, default=False, editable=False)
    # creador = models.CurrentUserField()

    estado = models.CharField(max_length=15, choices=TIPO_PERMISO_ESTADO, verbose_name='Estado')
    # aprobado = models.BooleanField(choices=BOOL_APROBADO, default=False)

    # autor = models.ForeignKey(User, related_name='created_by')
    # activo = models.BooleanField(default=False)
    fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', default=now)

    observacion_rrhh = models.TextField(blank=True, null=True)
    # fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', auto_now_add=True)
    # fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', auto_now_add=True)
    # fecha_registro = models.DateTimeField(verbose_name='Fecha de Solicitud', auto_now_add=True, editable=False)
    # --------------
    # objects = PermisoActivosPorUsuario()


    class Meta:
        permissions = (
            ("view_designacionpermiso", "Puede ver los registros"),
            ("options_designacionpermiso", "Puede ver los options"),
        )

    def clean(self, *args, **kwargs):
        if self.tipo_permiso == "comision" and not self.motivo:
            raise ValidationError('Debe ingresar el motivo de la comisión')

        if self.tipo_permiso == "comision" and not self.comision_lugar:
            raise ValidationError('Debe ingresar el lugar donde se realizo la salida en comisión')

        if self.tipo_permiso == "comision":
            if not self.hora_ini or not self.hora_fin:
                raise ValidationError('Debe ingresar las horas de salida y retorno de la comisión')

        if self.tipo_permiso == "particular":
            if not self.hora_ini or not self.hora_fin:
                raise ValidationError('Debe ingresar las horas de salida y retorno del permiso particular')

        if self.tipo_permiso == "particular" and not (self.particular_numeral_iii or self.particular_numeral_iv):
            raise ValidationError('Debe ingresar el motivo del permiso art. 31, numeral III o numeral VI')

        if self.tipo_permiso == "licencia" and not self.motivo:
            raise ValidationError('Debe ingresar el motivo de la licencia')

        if self.tipo_permiso == "licencia" and not self.licencia_respaldo:
            raise ValidationError('Debe ingresar el título/código del documento de respaldo')

        if self.fecha_ini > self.fecha_fin:
            raise ValidationError('Error la fecha de inicio debe ser menor o igual a la fecha final')

        if self.hora_ini and not self.hora_fin:
            raise ValidationError('Si registra una hora de inicio debe colocar una hora final')

        if self.hora_ini and self.hora_fin:
            if self.hora_ini > self.hora_fin:
                raise ValidationError('Error la hora de inicio debe ser menor o igual a la hora final')

        super(DesignacionPermiso, self).clean(*args, **kwargs)

    # def save(self, *args, **kwargs):
    #     inicio_mes = datetime(timezone.now().year, timezone.now().month, 1, 0, 0, 0, tzinfo=timezone.get_current_timezone())
    #
    #     if self.fecha_registro < inicio_mes:
    #         raise ValidationError('Error la fecha de registro debe ser mayor o igual al mes que se esta efectuando el permiso')
    #
    #     super(DesignacionPermiso, self).save(*args, **kwargs)

    # def save(self, *args, **kwargs):
    #     # COLOCAR AQUI CONSUMIR EL REST DE ORGANIGRAMA
    #     # Envio un uid y me devuelve el uid del jefe, si este esta inactivo me devuelve la del superior
    #     # self.jefe = User.objects.get(pk=1)
    #
    #     # Crear un Token Para confirmacion de Permiso mediante correo, por temas de la url cambio el separador por -
    #     # from django.core.signing import TimestampSigner
    #     # signer = TimestampSigner(salt="4g3t1c")
    #     # self.token_mail = signer.sign(self.pk)
    #
    #     # if not self.aprobado:
    #     #     raise ValidationError('Aprobado')
    #
    #     super(DesignacionPermiso, self).save(*args, **kwargs)

    def __str__(self):
        return "%s: %s - %s" % (self.tipo_permiso, self.fecha_ini, self.fecha_fin)

    def __unicode__(self):
        return "%s: %s - %s" % (self.tipo_permiso, self.fecha_ini, self.fecha_fin)


BOOL_HORAMAXMIN = (
    (True, 'Validar Hora Maxima de Marcacion'),
    (False, 'Validar Hora Minima de Marcacion')
)

MESES = (('enero', 'ENERO'),
         ('febrero', 'FEBRERO'),
         ('marzo', 'MARZO'),
         ('abril', 'ABRIL'),
         ('mayo', 'MAYO'),
         ('junio', 'JUNIO'),
         ('julio', 'JULIO'),
         ('agosto', 'AGOSTO'),
         ('septiembre', 'SEPTIEMBRE'),
         ('octubre', 'OCTUBRE'),
         ('noviembre', 'NOVIEMBRE'),
         ('diciembre', 'DICIEMBRE'))


from multiselectfield import MultiSelectField


class ConfiguracionGlobalManager(models.Manager):
    def yearActivo(self):
	cglobal = self.all().first()
	XMESES = {1: 'enero', 2: 'febrero', 3: 'marzo', 4: 'abril', 5: 'mayo', 6: 'junio', 7: 'julio', 8: 'agosto', 9: 'septiembre', 10: 'octubre', 11: 'noviembre', 12:'diciembre'}
	if XMESES[timezone.now().month] in cglobal.form_enable_vacaciones:
	    year_calendar = timezone.now().year+1
	else:
	    year_calendar = timezone.now().year
        return year_calendar


class ConfiguracionGlobal(models.Model):
    horario_max_entrada = models.BooleanField(choices=BOOL_HORAMAXMIN, verbose_name='Marcaciones de Entrada', default=False)
    horario_max_salida = models.BooleanField(choices=BOOL_HORAMAXMIN, verbose_name='Marcaciones de Salida', default=False)
    horario_lunes = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_lunes')
    horario_martes = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_martes')
    horario_miercoles = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_miercoles')
    horario_jueves = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_jueves')
    horario_viernes = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_viernes')
    horario_sabado = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_sabado')
    horario_domingo = models.ForeignKey(Horario, blank=True, null=True, related_name='horario_domingo')
    form_enable_vacaciones = MultiSelectField(choices=MESES, verbose_name='Meses habilitados para programacion de vacaciones', help_text='Las casillas no seleccionadas seran consideradas para reprogramacion')
    objects = ConfiguracionGlobalManager()
    # max_edit_form_vacaciones = models.IntegerField(default=8, verbose_name='Maximo Editar Formulario Vaciones por Gestion')

    class Meta:
        permissions = (
            ("view_configuracionglobal", "Puede ver los registros"),
            ("options_configuracionglobal", "Puede ver los options"),
        )

# ESTO ME SERVIRA PARA COMPARTIR INFORMACION ENTRE LOS SERVIDORES UTILIZANDO JWT CON RS256
# class ClavePublica(models.Model):
#     clave = models.TextField()

# TENGO QUE CREAR OTRA APP DONDE ESTEN LOS MODULOS DEL OTRO SISTEMA
# QUE REGISTRE LAS IP DE LOS BIOMETRICOS Y LOS CONSUMA EN UNA BASE DE DATOS

# Datos de usuario
# nombre cargo foto idusuario, uid, email
# Estructura de Menu

class Sugerencia(models.Model):
    sugerencia = models.TextField()
    ipaddr = models.GenericIPAddressField(blank=True, null=True)
    fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', default=now)

    class Meta:
        permissions = (
            ("view_sugerencia", "Puede ver los registros"),
            ("options_sugerencia", "Puede ver los options"),
        )

    def __str__(self):
        return self.sugerencia

    def __unicode__(self):
        return self.sugerencia


TIPO_VACACION = (
    ('completo', 'Todo el Día'),
    ('medio', 'Medio Día'),
    ('horas', 'Horas')
)


TIPO_VACACION_ESTADO = (
    ('precreado', 'Previamente Creado'),
    ('preaprobado', 'Previamente Aprobado'),
    ('creado', 'Creado'),
    ('aprobado', 'Aprobado Jefe'),
    ('rechazado', 'Rechazado')
)

from datetime import datetime, date, timedelta
from math import floor
from business_calendar import Calendar, MO, TU, WE, TH, FR, SA, SU


def SumaVacacionesTipo(item, xcalendario):
    bdiff = 0
    if item.tipo_vacacion == 'completo':
        bdiff = xcalendario.busdaycount(item.fecha_ini, item.fecha_fin+timedelta(days=1))
    if item.tipo_vacacion == 'medio':
        bdiff = float(xcalendario.busdaycount(item.fecha_ini, item.fecha_fin+timedelta(days=1)))/2
    return bdiff


class DesignacionVacacionManager(models.Manager):
    def diasTotal(self, user_auth, extraitem=None, xid=None):
        cglobal = ConfiguracionGlobal.objects.all().first()
        xdia_name = {6: 'domingo', 0: 'lunes', 1: 'martes', 2: 'miercoles', 3: 'jueves', 4: 'viernes', 5: 'sabado'}
        ydia_name = {5: SU, 6: MO, 0: TU, 1: WE, 2: TH, 3: FR, 4: SA}
        xworkday = []
        xferiado = []
        for xdia in xdia_name:
            if getattr(cglobal, 'horario_%s' % xdia_name[xdia]):
                horario_dia_list = HorarioRango.objects.filter(horario=getattr(cglobal, 'horario_%s' % xdia_name[xdia]))
                xworkday.append(ydia_name[xdia])

        for ydia in Feriado.objects.all():
            xferiado.append(ydia.fecha)
        tiempo = timedelta(0)
        # print xferiado
        # xcalendar = Calendar(workdays=xworkday, holidays=xferiado)
        xcalendar = Calendar(workdays=xworkday)
        xdiff = 0

	year_calendar = ConfiguracionGlobal.objects.yearActivo()
        items = self.filter(usuario=user_auth, fecha_ini__year=year_calendar, fecha_fin__year=year_calendar, estado__in=['precreado', 'creado', 'aprobado']).exclude(pk=xid)
        # items = self.filter(usuario=user_auth, estado__in=['precreado', 'creado'])
        # print items
        for item in items:
            xdiff = xdiff + SumaVacacionesTipo(item, xcalendar)
        if extraitem:
            xdiff = xdiff + SumaVacacionesTipo(extraitem, xcalendar)
        return xdiff


class DesignacionVacacion(models.Model):
    tipo_vacacion = models.CharField(max_length=15, choices=TIPO_VACACION, verbose_name='Tipo')
    usuario = models.ForeignKey(User)
    fecha_ini = models.DateField(verbose_name='Fecha Inicial (DD/MM/YYYY)')
    fecha_fin = models.DateField(verbose_name='Fecha Final (DD/MM/YYYY)')
    # hora_ini = models.TimeField(blank=True, null=True, verbose_name='Hora de salida (HH:MM)')
    # hora_fin = models.TimeField(blank=True, null=True, verbose_name='Hora de retorno (HH:MM)')
    jefe = models.ForeignKey(User, related_name='jefe_vacacion')
    unidad_organizacional = models.ForeignKey(UnidadOrganizacional, verbose_name='Unidad organizacional')
    estado = models.CharField(max_length=15, choices=TIPO_VACACION_ESTADO, verbose_name='Estado')
    fecha_registro = models.DateTimeField(verbose_name='Fecha de Registro', default=now)
    objects = DesignacionVacacionManager()

    class Meta:
        permissions = (
            ("view_vacacion", "Puede ver los registros"),
            ("options_vacacion", "Puede ver los options"),
        )

    def __str__(self):
        return "%s: %s - %s" % (self.tipo_vacacion, self.fecha_ini, self.fecha_fin)

    def __unicode__(self):
        return "%s: %s - %s" % (self.tipo_vacacion, self.fecha_ini, self.fecha_fin)

